# Risssk

**Risssk** is a digital version of the famous board game Risk.

## Authors 

Cl�mence Bellon, Corentin Friedrich and Lucas Martelet, ENSG's students.

## Run

Just open a command window, go in the directory and execute the following command: **java -jar Risk.jar**
The minimal version of the JRE is the 1.8 to have JavaFx.
You can also launch it on eclipse or a similar software.
***Warning***: the package **JavaFX** must be installed on eclipse. If not you can install here: [JavaFX Developper Home](https://www.oracle.com/technetwork/java/javase/overview/javafx-overview-2158620.html).


## How to play

To launch a new game, please click on "Nouvelle Partie", then choose the number of players you are and click on "Valider", then create your players and validate them.
You can now really play ; the icon of the active player is on the left. 
You can at your turn add the troops of your stock ("r�serve") on a district by clicking on it, then on "Lancer une Action", then choosing the number of troops you want and clicking on "Recruter sur la province". 
If you want to conquer an enemy district, left-click on one of you districts, then right-click on your target.; select the number of battalions you want to send and click on "Lancer les troupes".
Idem if you want to move your troops through your empire, just check you own the target.
You can open the bonus window to add more troops with a combination of bonus cards you earned from your previous conquests - you can't have more than 5 cards in your deck.
In case of ragequit, don't worry you can save your game and load it later.

Have fun and happy annihilation of your former friends.
ProTip: be sneaky, make alliances but most of all beak them !

## Support

If you have any question you can contact us at the following adress: corentin.friedrich@ensg.eu

## Contributing

You are free to add other maps. For that you just have to replace the map and  change the files **Continents**, **Provinces** and **Bonus**, but it is very important to let them at the same emplacement.

## Project status

At the beginning we wanted to add event cards that could affect the game as in some versions of the Risk ([alternate version](https://en.wikipedia.org/wiki/Risk:_The_Lord_of_the_Rings_Trilogy_Edition)) but we didn't have the time to implement it.



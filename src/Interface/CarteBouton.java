package Interface;

import javafx.scene.control.Button;
import Modele.Bonus;

/**
 * Classe qui represente le bouton associe a une carte bonus
 *
 * @author AsTeRisk
 */
public class CarteBouton extends Button {
    /**
     * Carte bonus associe au bouton
     */
    private Bonus bonus;

    /**
     * Constructeur de la classe avec un bonus en parametre
     *
     * @param bonus Carte bonus que l'on souhaite associe au bouton
     */
    public CarteBouton(Bonus bonus) {
        super();
        this.bonus = bonus;
    }

    /**
     * Accesseur du bonus
     *
     * @return le bonus associe au bouton
     */
    public Bonus getBonus() {
        return bonus;
    }

}



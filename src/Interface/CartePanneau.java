package Interface;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

/**
 * Classe qui represente le panneau associe a une carte
 *
 * @author AsTeRisk
 */
public class CartePanneau extends AnchorPane {

    /**
     * bouton asscoie a la carte
     */
    private CarteBouton bouton;
    /**
     * rectangle decoratif de fond
     */
    private Rectangle rectangle;
    /**
     * rectangle decoratif des bords
     */
    private Rectangle rectangleBords;
    /**
     * nom de la province associee a la carte bonus representee par le panneau
     */
    private Label text;
    /**
     * ImageView dans laquelle se trouve l'image du type de regiment associe a la carte
     */
    private ImageView vueCarte;

    /**
     * Constructeur du panneau avec le bouton de la carte bonus representee
     *
     * @param bouton bouton de la carte bonus qui va etre representee sur le tableau
     */
    public CartePanneau(CarteBouton bouton) {
        this.vueCarte = new ImageView();
        this.bouton = bouton;
        this.rectangle = new Rectangle();
        this.rectangle.setFill(Color.rgb(192, 186, 181));
        this.rectangle.setArcHeight(30);
        this.rectangle.setArcWidth(30);
        this.rectangleBords = new Rectangle();
        this.rectangleBords.setFill(null);
        this.rectangleBords.setStrokeWidth(3);
        this.rectangleBords.setArcHeight(30);
        this.rectangleBords.setArcWidth(30);
        this.rectangleBords.setStroke(Color.rgb(55, 46, 40));
        this.text = new Label();
        this.text.setText(this.bouton.getBonus().getNomProvince());
        this.text.setLayoutX(70);
        this.text.setLayoutY(90);
        this.text.setFont(new Font("Tahoma", 25));
        this.text.setTextFill(Color.rgb(71, 64, 55));

        switch (this.bouton.getBonus().getCategorieBataillon()) {
            case TroupierDeBase:

                //Pas oublier d'adapter le chemin
                this.setHeight(440);
                this.setWidth(220);
                this.vueCarte.setLayoutX(90);
                this.vueCarte.setLayoutY(140);
                this.vueCarte.setImage(new Image("file:./graphisme/Soldat/SoldatGris.png", 190, 380, false, true));
                break;
            case CanonFarceur:

                //Pas oublier d'adapter le chemin
                this.setHeight(310);
                this.setWidth(358);
                this.vueCarte.setLayoutX(45);
                this.vueCarte.setLayoutY(220);
                this.vueCarte.setImage(new Image("file:./graphisme/Canon/canonGris.png", 240, 195, false, true));
                //Pour une image de fond
                //g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
                break;
            case CavalierSuicidaire:
                //Pas oublier d'adapter le chemin
                this.setHeight(310);
                this.setWidth(251);
                this.vueCarte.setLayoutX(45);
                this.vueCarte.setLayoutY(150);
                this.vueCarte.setImage(new Image("file:./graphisme/Cavalier/CavalierGris.png", 260, 300, false, true));
                //Pour une image de fond
                //g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
                break;

        }

        this.rectangle.setHeight(519 - 20);
        this.rectangle.setWidth(519 * 57 / 88 - 20);
        this.rectangle.setLayoutX(10);
        this.rectangle.setLayoutY(10);
        this.rectangleBords.setLayoutX(40);
        this.rectangleBords.setLayoutY(40);
        this.rectangleBords.setHeight(519 - 80);
        this.rectangleBords.setWidth(519 * 57 / 88 - 80);
        this.bouton.setLayoutY(0);
        this.bouton.setOpacity(0);
        this.bouton.setPrefWidth(519 * 57 / 88);
        this.bouton.setPrefHeight(519);
        this.getChildren().add(this.rectangle);
        this.getChildren().add(this.rectangleBords);
        this.getChildren().add(this.vueCarte);
        this.getChildren().add(this.text);
        this.getChildren().add(this.bouton);
    }
}

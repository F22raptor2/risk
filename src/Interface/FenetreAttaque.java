package Interface;

import java.awt.Color;
import java.awt.Font;
import java.awt.Label;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Classe decrivant les fenetres d'attaque
 *
 * @author AsTeRisk
 */

public class FenetreAttaque extends JFrame {

    /**
     * Le panneau d'attaque correspondant
     */
    private PanneauAttaque pana;
    /**
     * La liste des listes successives des resultats aux lancers de des d'attaque
     */
    private List<List<Integer>> listeDesAttaque;
    /**
     * La liste des listes successives des resultats aux lancers de des de defense
     */
    private List<List<Integer>> listeDesDefense;
    /**
     * Si l'attaque a reussi
     */
    private boolean reussite;

    /**
     * Generateur de fenetre d'attaque
     *
     * @param listeDesAttaque La liste des listes successives des resultats aux lancers de des d'attaque
     * @param listeDesDefense La liste des listes successives des resultats aux lancers de des de defense
     * @param chemin          L'adresse du dossier contenant toutes les illustrations utilisees dans le jeu
     * @param reussite        Si l'attaque a reussi
     */
    public FenetreAttaque(List<List<Integer>> listeDesAttaque, List<List<Integer>> listeDesDefense, String chemin, boolean reussite) {

        this.listeDesAttaque = listeDesAttaque;
        this.listeDesDefense = listeDesDefense;
        this.reussite = reussite;

        // On donne a la fenetre un titre, sa taille, sa position - au centre de l'Ã©cran
        this.setTitle("Attaque!");
        this.setSize(600, 700);
        this.setLocationRelativeTo(null);

        // On cree un panneau d'attaque,
        this.pana = new PanneauAttaque();
        pana.setSpoil(false); // Permet de ne pas afficher le resultat de l'attaque avant la fin de l'animation
        pana.setReussite(reussite);
        pana.setChemin(chemin);

        // On dit a la fenetre que son panneau est pana
        this.setContentPane(pana);

        // On rend la fenetre visible
        this.setVisible(true);

        // Appel a go
        lancerAnimation();
    }


    /**
     * Methode qui modifie le panneau et gere le temps pour creer une animation
     */
    private void lancerAnimation() {
        for (int i = 0; i < this.listeDesAttaque.size(); i++) {

            //On donne au panneau les resultats des des lors du ieme lancer
            pana.setDesAttaque(this.listeDesAttaque.get(i));
            pana.setDesDefense(this.listeDesDefense.get(i));
            for (int j = 0; j < 250; j++) {
                //On change j, donc la position des des
                pana.setJ(j);
                //On demande au panneau de se redessiner en prenant en compte ses nouveaux parametres
                pana.repaint();
                //On attend 2 ms avant de continuer
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //Quand les des ont fini leur deplacement, on attend 1,5 sec avant de continuer
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //L'animation est finie, on peut reveler le resultat de l'attaque
        pana.setSpoil(true);

        //On demande au panneau de se redessiner pour qu'il fasse apparaitre le resultat
        pana.repaint();
    }

}

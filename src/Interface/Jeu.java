package Interface;

import com.sun.org.apache.xpath.internal.operations.Bool;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import Modele.*;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Classe controleur de la partie interface du plateau, elle contient entre autres des informations sur le monde de jeu et sur le joueur
 * dont c'est le tour
 *
 * @author AsTeRisk
 */
public class Jeu implements Initializable {

    /**
     * Scene sur laquelle vont se greffer les differents panneaux
     */
    private Scene scene;
    /**
     * Le monde de jeu qui va etre modifie lors de la partie
     */
    private Monde MondeDeJeu;
    /**
     * Panneau contenant les differents boutons aococies aux provinces ainsi que les images des pions presents sur celles-ci
     */
    @FXML
    private AnchorPane Provinces;
    /**
     * Panneau pour quitter et sauveagrder la partie
     */
    @FXML
    private AnchorPane panneauQuitter;
    /**
     * ImageView qui montre la tete du joueur dont c'est actuellemnt le tour
     */
    @FXML
    private ImageView imageJoueur;
    /**
     * Label affichant la quantite de soldat dans la reserve du joueur
     */
    @FXML
    private Label reserve;
    /**
     * Panneau du deck du joueur permettant d'utiliser des cartes bonus
     */
    @FXML
    private AnchorPane panneauDeck;
    /**
     * Panneau permettant d'effectuer les actions suivantes : attaquer, deplacer et placer des renforts
     */
    @FXML
    private AnchorPane panneauAction;
    /**
     * ComboBox qui indique le nombre de regiment pouvant etre deployer depuis la province de depart selectionnee vers une autre
     */
    @FXML
    private ComboBox<Integer> nombreSoldat;
    /**
     * Label qui affiche un texte d'alerte en cas d'erreur lors d'une action en donnant une description de l'erreur (nombre de carte invalide,
     * nombre de regiment manquant, province de depart appartenant a une autre joueur,...)
     */
    @FXML
    private Label textAlerte;
    /**
     * ComboBox qui indiquant le nombre de regiment pouvant etre deployer depuis la reserve vers une province
     */
    @FXML
    private ComboBox<Integer> nombreSoldatReserve;
    /**
     * Panneau qui s'affiche lors d'une erreur de valeur donnée par l'utilisateur avec une description sommaire de l'erreur (voir textAlerte)
     */
    @FXML
    private AnchorPane panneauAlerte;
    /**
     * Panneau contenant les differentes cartes du joueur actif
     */
    @FXML
    private AnchorPane panneauCarte;
    /**
     * TextField qui permet a l'utilisateur de nommer sa sauvegarde
     */
    @FXML
    private TextField nomSauvegarde;
    /**
     * Panneau permettant de sauvegarder la partie en cours
     */
    @FXML
    private AnchorPane panneauSauvegarde;
    /**
     * ImageView qui affiche la carte
     */
    @FXML
    private ImageView carte;
    /**
     * ImageView qui s'affiche lorsqu'un joueur gagne la partie
     */
    @FXML
    private ImageView gagne;

    /**
     * Reserve du joueur dont c'est le tour
     */
    private Reserve reserveJoueur;
    /**
     * Bouton associe a la province selectionnee comme etantcelle de depart
     */
    private ProvinceBouton provinceDepart;
    /**
     * Bouton associe a la province selectionnee comme etantcelle d'arrivee
     */
    private ProvinceBouton provinceArrivee;
    /**
     * Joueur dont c'est le tour
     */
    private Joueur joueurActif;
    /**
     * Nombre de soldat selectionne dans la ComboBox nombreSoldat
     */
    private Integer soldatEnvoye;
    /**
     * Nombre de soldat selectionne dans la ComboBox nombreSoldatReserve
     */
    private Integer soldatRecrute;
    /**
     * Combinaison selectionnee par le joueur dans le panneau Deck
     */
    private Deck combinaisonCarte;
    /**
     * Boolean qui indique si la partie a ete sauvegarder ou non, utilise dans la methode SauvegardeEtQuitter
     */
    private Boolean sauvegarde;
    /**
     * Boolean qui indique si la main du joueur est pleine ou non, utilise lors de la pioche d'une carte pour forcer le joueur a s'en defausser d'une
     */
    private Boolean mainPleine;
    /**
     * Boolean qui indique si la derniere attaque du joueur a ete une ruessite (true) ou non (false)
     */
    private Boolean reussite;

    /**
     * Methode d'initialisation du controleur
     *
     * @param location
     * @param resources
     */


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.mainPleine = false;
        this.sauvegarde = false;
        this.reussite = false;
        this.MondeDeJeu = Menu.Mondefinal;
        //Creation des boutons de provinces
        for (Province prov : this.MondeDeJeu.getProvinces()) {
            AnchorPane province = new AnchorPane();
            ProvinceBouton bouton = new ProvinceBouton(prov);
            province.setLayoutX(prov.getLayoutx());
            province.setLayoutY(prov.getLayouty());
            bouton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    if (mouseEvent.getButton() == MouseButton.PRIMARY) {
                        selectionProvinceDepart(bouton);
                    } else if (mouseEvent.getButton() == MouseButton.SECONDARY) {
                        selectionProvinceArrivee(bouton);
                    }
                }
            });
            AnchorPane imagesPions = new AnchorPane();
            miseAJourVisuelleProvince(prov, imagesPions);
            province.getChildren().add(imagesPions);
            province.getChildren().add(bouton);


            this.Provinces.getChildren().add(province);
        }
        this.joueurActif = this.MondeDeJeu.getJoueurs().get(this.MondeDeJeu.getJoueurTour());
        this.imageJoueur.setImage(new Image("file:./graphisme/Perso/Perso" + this.joueurActif.getCouleur() + ".png", 200, 200, true, true));
        this.reserveJoueur = this.joueurActif.getConscription();
        miseAJourReserve();
        this.nombreSoldatReserve.valueProperty().addListener(observable -> setSoldatRecrute(this.nombreSoldatReserve.getValue()));
        this.miseAJourMain();
        this.carte.setImage(new Image("file:./graphisme/riskMap.png", 1821, 1024, true, true));
    }


    /**
     * Mutateur de l'attribut soldatEnvoye
     *
     * @param soldatEnvoye Entier qui va etre la nouvele valeur de l'attribut. Il est definit par la valeur selectionnee dans la ComboBox nombreSoldat
     */
    public void setSoldatEnvoye(Integer soldatEnvoye) {
        this.soldatEnvoye = soldatEnvoye;
    }

    /**
     * Mutateur de l'attribut soldatRecrute
     *
     * @param soldatRecrute Entier qui va etre la nouvele valeur de l'attribut. Il est definit par la valeur selectionnee dans la ComboBox nombreSoldatReserve
     */
    public void setSoldatRecrute(Integer soldatRecrute) {
        this.soldatRecrute = soldatRecrute;
    }

    /**
     * Methode qui ouvre le panneau Quitter
     *
     * @param actionEvent Tout clic sur le bouton Quitter
     */
    public void PanneauQuitter(ActionEvent actionEvent) {
        this.panneauQuitter.setVisible(true);
    }

    /**
     * Methode qui permet de quitter le jeu en quittant la fenetre
     *
     * @param actionEvent Tout clic sur le bouton associe
     */
    public void Quitter(ActionEvent actionEvent) {
        //On recupere la fenetre dans laquelle se trouve le jeu
        Stage stage = (Stage) this.panneauAlerte.getScene().getWindow();
        // On ferme la fenetre
        stage.close();
    }

    /**
     * Methode qui lance le panneau sauvegarde. Elle modifie aussi l'attribut sauvegarde pour que le jeu se ferme apres la validation de la
     * sauvegarde
     *
     * @param actionEvent Tout clic sur le bouton Sauvegarder et Quitter
     */
    public void SauvegardeEtQuitter(ActionEvent actionEvent) {
        this.sauvegarde = true;
        this.panneauSauvegarde.setVisible(true);
    }

    /**
     * Methode qui masque le panneau Quitter
     *
     * @param actionEvent Tout clic sur le bouton Retour du panneau Quitter
     */
    public void RetourPanneauQuitter(ActionEvent actionEvent) {
        this.panneauQuitter.setVisible(false);
    }

    /**
     * Methode qui permet d'ouvrir le panneau Deck du joueur actif
     *
     * @param actionEvent Tout clic sur le bouton Deck
     */
    public void OuvrirDeck(ActionEvent actionEvent) {
        this.panneauDeck.setVisible(true);
    }

    /**
     * Methode qui permet de reinitialiser la combinaison de carte en cour
     *
     * @param actionEvent tout clic sur le bouton Annuler la selection
     */
    public void AnnulerSelection(ActionEvent actionEvent) {
        this.combinaisonCarte = new Deck();
        this.miseAJourMain();
    }

    /**
     * Methode qui permet de defausser la combinaison de carte en cour
     *
     * @param actionEvent tout clic sur le bouton Defausser les cartes selectionnees
     */
    public void DeffausserCartes(ActionEvent actionEvent) {
        //selection des cartes de la combinaisons selectionnee et ajout de celles-ci a la pioche
        for (Bonus carte : this.combinaisonCarte.getMainBonus()) {
            this.joueurActif.getDeck().getMainBonus().remove(carte);
            this.MondeDeJeu.getPioche().getPonussss().add(carte);
        }
        this.combinaisonCarte = new Deck();
        this.miseAJourMain();
        if (this.joueurActif.getDeck().getMainBonus().size() == 5) {
            this.mainPleine = true;
        } else {
            this.mainPleine = false;
        }
    }

    /**
     * Methode qui permet d'ajouter une carte a la combinaison actuelle. Elle modifie l'aspect visuelle de la carte selectionnee
     *
     * @param carte
     */
    private void ajoutCarteCombinaison(CarteBouton carte) {
        if (this.combinaisonCarte == null) {
            this.combinaisonCarte = new Deck();
        }
        this.combinaisonCarte.getMainBonus().add(carte.getBonus());
        carte.setOpacity(0.4);
        carte.setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    /**
     * Methode qui permet d'utiliser une combinaison de carte pour obtenir des renfort supplementaires
     *
     * @param actionEvent tout clic sur le bouton Valider Combinaison
     */
    public void ValiderCombinaison(ActionEvent actionEvent) {
        //Gestion de l'erreur en cas de nombre de carte invalide
        if (this.combinaisonCarte.getMainBonus().size() != 3) {
            textAlerte.setText("Le nombre de carte est invalide");
            textAlerte.setVisible(true);
            panneauAlerte.setVisible(true);
        } else {
            Map<Integer, List<Bonus>> bonus = this.combinaisonCarte.calculerNouveauBataillon();
            //Si la combinaison n'aboutit a aucun bonus
            if (bonus.size() == 0) {
                textAlerte.setText("La combinaison est invalide");
                panneauAlerte.setVisible(true);
            } else {
                this.reserveJoueur.addBonusCarte(bonus);
                this.miseAJourMain();
                this.reserve.setText("Reserve :" + this.reserveJoueur.getCaserne());
            }
        }
    }

    /**
     * Methode qui permet de masquer le panneau Deck du joueur actif
     *
     * @param actionEvent Tout clic sur le bouton Cacher du panneau Deck
     */
    public void cacherPanneauDeck(ActionEvent actionEvent) {
        if (this.mainPleine) {
            this.textAlerte.setText("Attention vous avez trop de carte en main vouss devez en defausser une");
            this.panneauAlerte.setVisible(true);
        } else if (this.reussite) {
            this.joueurActif.getDeck().piocher(this.MondeDeJeu.getPioche());
            this.reussite = false;
            this.panneauDeck.setVisible(false);
            miseAJourMain();
        } else {

            this.panneauDeck.setVisible(false);
        }
    }

    /**
     * Methode qui permet de selectionner la province de depart de toute action (la province d'ou vont partir les soldats pour un deplacement ou
     * une attaque ou la province ou le recrutement va se faire)
     * Elle va aussi changer l'aspect du bouton associe a la province selectionnee
     *
     * @param bouton Clic gauche de la souris sur un bouton de Province
     */
    public void selectionProvinceDepart(ProvinceBouton bouton) {
        //Deselection de la province prealablement selectionnee
        if (this.provinceDepart != null) {
            this.provinceDepart.setOpacity(0.2);
            this.provinceDepart.setBackground(new Background(new BackgroundFill(Color.LIGHTGREY, CornerRadii.EMPTY, Insets.EMPTY)));
            this.nombreSoldat.getItems().clear();
        }
        //Selection de la nouvelle province et modification de l'aspect de son bouton
        this.provinceDepart = bouton;
        Province province = this.provinceDepart.getProvince();
        miseAJourProvince(province);
        bouton.setOpacity(0.7);
        bouton.setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    /**
     * Methode qui permet de selectionner la province d'arrivee des actions de deplacement et d'attaque
     * Elle va aussi changer l'aspect du bouton associe a la province selectionnee
     *
     * @param bouton Clic droit de la souris sur un bouton de Province
     */
    public void selectionProvinceArrivee(ProvinceBouton bouton) {
        //Deselection de la province prealablement selectionnee
        if (this.provinceArrivee != null) {
            this.provinceArrivee.setOpacity(0.2);
            this.provinceArrivee.setBackground(new Background(new BackgroundFill(Color.LIGHTGREY, CornerRadii.EMPTY, Insets.EMPTY)));
        }
        //Selection de la nouvelle province et modification de l'aspect de son bouton
        this.provinceArrivee = bouton;
        bouton.setOpacity(0.7);
        bouton.setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    /**
     * Methode qui ouvre le panneau d'action
     *
     * @param actionEvent Tout clic sur le bouton Lancer une action
     */
    public void OuverturePanneauAction(ActionEvent actionEvent) {
        this.panneauAction.setVisible(true);
    }

    /**
     * Methode qui va permettre de lancer des troupes de la province de depart selectionnee a la province d'arrivee selectionnee. Suivant le
     * proprietaire de la province d'arrivee, cette methode va permettre soit l'attaque (proprietaire different de la province de depart) soit
     * le deplacement (meme proprietaire pour les deux provinces). Le nombre de regiments mobilise est celui selectionne par la ComboBox
     * nombreSoldat
     *
     * @param actionEvent tout clic sur le bouton Lancer des troupes
     */
    public void LancerTroupes(ActionEvent actionEvent) {
        //Gestion des exceptions en cas d'abscence de certain parametre
        if (this.soldatEnvoye == null || this.provinceDepart == null || this.provinceArrivee == null || this.nombreSoldat == null) {
            this.textAlerte.setText("Attention il manque la province de départ ou d'arrivée ou aucun régiment n'est envoyé ou aucun régiment n'est diponible");
            this.panneauAlerte.setVisible(true);
            //Gestion des exception en cas de non voisinage des provinces
        } else if (!this.provinceDepart.getProvince().getVoisins().contains(this.provinceArrivee.getProvince())) {
            this.textAlerte.setText("Les provinces ne sont pas voisines");
            this.panneauAlerte.setVisible(true);
            //Gestion du cas ou la province de depart n'appartient pas au joueur actif
        } else if (this.provinceDepart.getProvince().getGouvernant() != this.joueurActif) {
            this.textAlerte.setText("La province ne t'appartient pas");
            this.panneauAlerte.setVisible(true);
            //Si les deux provinces selectionnees sont voisines, utilisation de la fonction deplacement
        } else if (this.provinceDepart.getProvince().getGouvernant().equals(this.provinceArrivee.getProvince().getGouvernant())) {
            this.provinceDepart.getProvince().deplacer(this.provinceArrivee.getProvince(), this.soldatEnvoye);
            //selection du panneau de la province de depart pour mettre a jour les regiments presents
            AnchorPane panneauProvince = (AnchorPane) this.Provinces.getChildren().get(this.provinceDepart.getProvince().getIdent() - 1);
            AnchorPane panneauRegimentProvince = (AnchorPane) panneauProvince.getChildren().get(0);
            panneauRegimentProvince.getChildren().clear();
            miseAJourVisuelleProvince(provinceDepart.getProvince(), panneauRegimentProvince);
            //selection du panneau de la province d'arrivee pour mettre a jour les regiments presents
            AnchorPane panneauProvinceArrivee = (AnchorPane) this.Provinces.getChildren().get(this.provinceArrivee.getProvince().getIdent() - 1);
            AnchorPane panneauRegimentProvinceArrivee = (AnchorPane) panneauProvinceArrivee.getChildren().get(0);
            panneauRegimentProvinceArrivee.getChildren().clear();
            miseAJourVisuelleProvince(provinceArrivee.getProvince(), panneauRegimentProvinceArrivee);
            //mise a jour de la combobox de selection de soldat
            miseAJourProvince(this.provinceDepart.getProvince());
            //Si il y a un gagnant
            if (this.MondeDeJeu.finDeJeu()) {
                this.gagne.setVisible(true);
            }
            //Si les deux provinces n'ont pas le meme proprietaire, utilisation de la fonction attaque
        } else if (this.provinceDepart.getProvince().getGouvernant().getCouleur() != this.provinceArrivee.getProvince().getGouvernant().getCouleur()) {
            this.reussite = this.provinceDepart.getProvince().attaquer(this.provinceArrivee.getProvince(), this.soldatEnvoye, this.MondeDeJeu.getContinents(), this.MondeDeJeu.getPioche());
            //si l'attaque est une reussite et que la main du joueur est pleine, gestion de la defausse obligatoire d'une de ses cartes
            if (this.joueurActif.getDeck().getMainBonus().size() == 5 && reussite) {
                this.mainPleine = true;
                this.panneauDeck.setVisible(true);
            }
            //selection du panneau de la province de depart pour mettre a jour les regiments presents
            AnchorPane panneauProvince = (AnchorPane) this.Provinces.getChildren().get(this.provinceDepart.getProvince().getIdent() - 1);
            AnchorPane panneauRegimentProvince = (AnchorPane) panneauProvince.getChildren().get(0);
            panneauRegimentProvince.getChildren().clear();
            miseAJourVisuelleProvince(provinceDepart.getProvince(), panneauRegimentProvince);
            //selection du panneau de la province d'arrivee pour mettre a jour les regiments presents
            AnchorPane panneauProvinceArrivee = (AnchorPane) this.Provinces.getChildren().get(this.provinceArrivee.getProvince().getIdent() - 1);
            AnchorPane panneauRegimentProvinceArrivee = (AnchorPane) panneauProvinceArrivee.getChildren().get(0);
            panneauRegimentProvinceArrivee.getChildren().clear();
            miseAJourVisuelleProvince(provinceArrivee.getProvince(), panneauRegimentProvinceArrivee);
            miseAJourMain();
            //mise a jour de la combobox de selection de soldat
            miseAJourProvince(this.provinceDepart.getProvince());
        } else {
            this.textAlerte.setText("La province ne t'appartient pas");
            this.panneauAlerte.setVisible(true);
        }

    }

    /**
     * Methode qui va permettre de recruter des unites sur la province de depart prealablement selectionnee. Le nombre de regiments recrute
     * est celui selectionne dans la ComboBox nombreSoldatReserve
     *
     * @param actionEvent tout clic sur le bouton Recruter  sur la province
     */
    public void LancerRecrutement(ActionEvent actionEvent) {
        //Si certain parametre ne sont pas definis
        if (this.soldatRecrute == null || this.provinceDepart == null || this.nombreSoldatReserve == null) {
            this.textAlerte.setText("Attention la province n'est pas définit, aucun régiment n'est envoyé ou votre réserve est vide");
            this.panneauAlerte.setVisible(true);
        } else if (this.provinceDepart.getProvince().getGouvernant().equals(this.joueurActif)) {
            this.joueurActif.placerRenfort(this.soldatRecrute, this.provinceDepart.getProvince());
            //selection du panneau de la province pour mettre a jour les regiments presents
            AnchorPane panneauProvince = (AnchorPane) this.Provinces.getChildren().get(this.provinceDepart.getProvince().getIdent() - 1);
            AnchorPane panneauRegimentProvince = (AnchorPane) panneauProvince.getChildren().get(0);
            panneauRegimentProvince.getChildren().clear();
            miseAJourVisuelleProvince(provinceDepart.getProvince(), panneauRegimentProvince);
            miseAJourReserve();
            //si la province n'appartient pas au joueur actif
        } else {
            this.textAlerte.setText("La province ne t'appartient pas");
            this.panneauAlerte.setVisible(true);
        }
    }

    /**
     * Methode qui va fermer le panneau d'action
     *
     * @param actionEvent Tout clic sur le bouton Retour a la carte du monde du panneau action
     */
    public void FermeturePanneauAction(ActionEvent actionEvent) {
        this.panneauAction.setVisible(false);
    }

    /**
     * Methode qui ferme le panneau Alerte
     *
     * @param actionEvent Tout clic sur le bouton Fermer l'alerte
     */
    public void FermerTextAlerte(ActionEvent actionEvent) {
        this.panneauAlerte.setVisible(false);
    }

    /**
     * Methode qui ouvre le panneau de sauvegarde
     *
     * @param actionEvent tout clic sur le bouton sauvegarde
     */
    public void ouvrirPanneauSauvegarde(ActionEvent actionEvent) {
        this.panneauSauvegarde.setVisible(true);
    }

    /**
     * Methode qui permet de sauvegarder la partie en cours
     *
     * @param actionEvent tout clic sur le bouton Sauvegarder
     */
    public void Sauvegarde(ActionEvent actionEvent) {
        this.MondeDeJeu.sauvegardePartie(this.nomSauvegarde.getCharacters().toString());
        if (sauvegarde) {
            this.Quitter(actionEvent);
        }
        this.panneauSauvegarde.setVisible(false);
    }

    /**
     * Methode qui ferme le panneau de sauvegarde
     *
     * @param actionEvent tout clic sur le bouton Cacher du panneau sauvegarde
     */
    public void quitterPanneauSauvegarde(ActionEvent actionEvent) {
        this.panneauSauvegarde.setVisible(false);
    }

    /**
     * Methode qui permet de mettre fin au tour du joueur actif et de passer au joueur suivant. Elle met a jour les attributs du controleur
     * lies au joueur actif
     *
     * @param actionEvent Tout clic sur le bouton Fin de Tour
     */
    public void FinTour(ActionEvent actionEvent) {
        this.MondeDeJeu.getJoueurs().get(this.MondeDeJeu.getJoueurTour()).finDeTour();
        //Changement de l'indice du joueur dont c'est le tour
        this.MondeDeJeu.setJoueurTour(this.MondeDeJeu.getJoueurTour() + 1);
        this.MondeDeJeu.setJoueurTour(this.MondeDeJeu.getJoueurTour() % this.MondeDeJeu.getJoueurs().size());
        this.joueurActif = this.MondeDeJeu.getJoueurs().get(this.MondeDeJeu.getJoueurTour());
        //Recrutement des effectifs pour le joueur
        this.joueurActif.getConscription().recruter();
        this.imageJoueur.setImage(new Image("file:./graphisme/Perso/Perso" + this.MondeDeJeu.getJoueurs().get(this.MondeDeJeu.getJoueurTour()).getCouleur() + ".png", 200, 200, true, true));
        this.reserveJoueur = this.MondeDeJeu.getJoueurs().get(this.MondeDeJeu.getJoueurTour()).getConscription();
        miseAJourReserve();
        this.nombreSoldatReserve.valueProperty().addListener(observable -> setSoldatRecrute(this.nombreSoldatReserve.getValue()));
        this.miseAJourMain();
        this.combinaisonCarte = new Deck();
    }

    /**
     * Methode qui met a jour la reserve du joueur actif
     */
    public void miseAJourReserve() {
        this.nombreSoldatReserve.getItems().clear();
        for (int k = 1; k <= this.reserveJoueur.getCaserne(); k++) {
            this.nombreSoldatReserve.getItems().add(k);
        }
        this.reserve.setText("Reserve :" + this.reserveJoueur.getCaserne());
    }

    /**
     * Methode qui met a jour la ComboBox de la province
     *
     * @param province province dont on souhaite faire la mise a jour
     */
    public void miseAJourProvince(Province province) {
        this.nombreSoldat.getItems().clear();
        for (int k = 1; k < province.getRegiments() - province.getRegimentsFatigues(); k++) {
            this.nombreSoldat.getItems().add(k);
        }
        this.nombreSoldat.valueProperty().addListener(observable -> setSoldatEnvoye(this.nombreSoldat.getValue()));
    }

    /**
     * Methode qui va modifie le nombre d'image de regiments present en fonction du nombre de regiment sur la province
     *
     * @param prov        province dont on va faire une mise a jour visuelle
     * @param imagesPions panneau contenant les images des pions presents sur la province
     */
    public void miseAJourVisuelleProvince(Province prov, AnchorPane imagesPions) {
        List<Integer> nombrePions = new ArrayList<>();
        int regimRestant;
        int artilerie = prov.getRegiments() / 5;
        regimRestant = prov.getRegiments() % 5;
        int cavalerie = regimRestant / 3;
        regimRestant = regimRestant % 3;
        int infanterie = regimRestant;
        for (int i = 0; i < artilerie; i++) {
            nombrePions.add(2);
        }
        for (int i = 0; i < cavalerie; i++) {
            nombrePions.add(1);
        }
        for (int i = 0; i < infanterie; i++) {
            nombrePions.add(0);
        }

        for (List<Integer> layout : this.MondeDeJeu.getLayoutProvince()) {
            ImageView imagePion = new ImageView();
            imagePion.setX(layout.get(0));
            imagePion.setY(layout.get(1));
            imagesPions.getChildren().add(imagePion);

        }
        for (int i = 0; i < nombrePions.size(); i++) {
            ImageView img = (ImageView) imagesPions.getChildren().get(i);
            if (nombrePions.get(i) == 0) {
                img.setImage(new Image("file:./graphisme/Soldat/Soldat" + prov.getGouvernant().getCouleur() + ".png", 40, 40, true, true));
            } else if (nombrePions.get(i) == 1) {
                img.setImage(new Image("file:./graphisme/Cavalier/Cavalier" + prov.getGouvernant().getCouleur() + ".png", 40, 40, true, true));
            } else if (nombrePions.get(i) == 2) {
                img.setImage(new Image("file:./graphisme/Canon/canon" + prov.getGouvernant().getCouleur() + ".png", 40, 40, true, true));
            }
        }
    }

    /**
     * Methode qui met a jour l'aspect visuel de la main du joueur
     */
    public void miseAJourMain() {
        this.panneauCarte.getChildren().clear();
        int k = 0;
        for (Bonus carte : this.joueurActif.getDeck().getMainBonus()) {
            CarteBouton boutonCarte = new CarteBouton(carte);
            AnchorPane vueCarte = new CartePanneau(boutonCarte);
            vueCarte.setLayoutX(k * 368);
            boutonCarte.setLayoutX(k * 368);
            this.panneauCarte.getChildren().add(vueCarte);
            boutonCarte.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    ajoutCarteCombinaison(boutonCarte);
                }
            });
            this.panneauCarte.getChildren().add(boutonCarte);
            k++;
        }
    }
}
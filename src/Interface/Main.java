package Interface;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import Modele.*;
import Sauvegarde.Docks;
import Sauvegarde.Save;

import java.util.List;
import java.util.Scanner;

/**
 * Classe main qui va etre lancer lors de l'execution du programme
 *
 * @author AsTeRisk
 */

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("menu.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root, 2100, 1000);
        primaryStage.setTitle("Risk");
        primaryStage.setScene(scene);
        Menu controleurMenu = fxmlLoader.<Menu>getController();
        controleurMenu.setScene(scene);

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }


    public static boolean finDeJeu(List<Joueur> protagonistes) {
        for (int i = 0; i < protagonistes.size(); i++) {
            if (protagonistes.get(i).getEmpire().size() == 42) {
                System.out.println("Le joueur " + protagonistes.get(i).getNom() + " a exterminé la vermine qui l'entourait! \n Bravo à lui c'est un exploit remarquable\n " +
                        "maintenant il n'a plus qu'à retourner à sa misérable petite vie dans la réalité réelle de la vérité véritable.");
                return true;
            }
        }
        return false;
    }
}




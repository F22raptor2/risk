package Interface;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import Modele.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


/**
 * Classe qui est le controleur du menu de jeu qui sert a lancer des parties
 *
 * @author AsTeRisk
 */
public class Menu implements Initializable {
    /**
     * La scene du controlleur, initialisee dans le main pour lancer l'application
     */
    private Scene scene;
    /**
     * Le monde de jeu qui va etre charger ou creer
     */
    private Monde monde;
    /**
     * Copie du monde de jeu accessible depuis d'autres controleurs
     */
    public static Monde Mondefinal;
    /**
     * Panneau utilise dans la creation d'une partie pour definir le nombre de joueur
     */
    @FXML
    private AnchorPane nombreJoueur;
    /**
     * Label qui affiche le nombre de joueurs actuellement selectionne
     */
    @FXML
    private Label indicNum;
    /**
     * Panneau initial qui est le premier affiche lors du lancement de l'application
     */
    @FXML
    private AnchorPane Debut;
    /**
     * Panneau de creation des joueurs
     */
    @FXML
    private AnchorPane creationJoueur;
    /**
     * TextField qui va permettre au joueur de rentrer le nom qu'il a choisit
     */
    @FXML
    private TextField nom;
    /**
     * Label qui affiche le nombre de joueur qu'il reste a creer avant de pouvoir lancer la partie
     */
    @FXML
    private Label textNombreJoueur;
    /**
     * Combobox qui permet au joueur de choisir la couleur qu'il souhaite avoir dans le jeu
     */
    @FXML
    private ComboBox<String> couleurs;
    /**
     * Combobox qui permet au joueur de choisir la sauvegarde qu'il souhaite charger
     */
    @FXML
    private ComboBox<String> sauvegardes;
    /**
     * Panneau de chargement des parties
     */
    @FXML
    private AnchorPane chargement;
    /**
     * ImageView qui va montrer l'aspect de la tete du joueur en fonction de la couleur qu'il a choisie
     */
    @FXML
    private ImageView teteJoueur;
    /**
     * Fond de la scene (uniquement esthetique)
     */
    @FXML
    private ImageView fond;
    /**
     * Image uniquement esthetique
     */
    @FXML
    private ImageView canon;
    /**
     * Image uniquement esthetique
     */
    @FXML
    private ImageView cavalier;
    /**
     * Image uniquement esthetique
     */
    @FXML
    private ImageView soldat;

    /**
     * Nombre de joueurs dans la partie
     */
    private int numJoueur;
    /**
     * Couleur selectionnee par le joueur en cours de creation
     */
    private String couleurJoueur;

    /**
     * Methode d'initialisation du controleur
     *
     * @param location
     * @param resources
     */


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.numJoueur = 0;
        this.monde = new Monde();
        this.teteJoueur.setImage(new Image("file:./graphisme/Perso/Perso.png", 200, 200, true, true));
        this.fond.setImage(new Image("file:./graphisme/logo_Risk.png", 2100 * 3 / 6, 0, true, true));
        this.canon.setLayoutX(1200);
        this.canon.setLayoutY(900 * 3 / 4 - 50);
        this.canon.setImage(new Image("file:./graphisme/Canon/canonTurquoise.png", 2100 * 3 / 10, 0, true, true));
        this.cavalier.setLayoutX(2100 / 11);
        this.cavalier.setLayoutY(1000 / 7);
        this.cavalier.setImage(new Image("file:./graphisme/Cavalier/CavalierVert.png", 0, 900 / 2, true, true));
        this.soldat.setLayoutX(2100 * 3 / 4);
        this.soldat.setLayoutY(900 / 4);
        this.soldat.setImage(new Image("file:./graphisme/Soldat/SoldatJaune.png", 2100 * 3 / 16, 0, true, true));
    }

    /**
     * Mutateur de l'attribut numJoueur
     *
     * @param k entier qui va etre la nouvelle valeur du nombre de joueur
     */
    public void setNombreJoueur(int k) {
        this.numJoueur = k;
        this.indicNum.setText("Nombre de joueurs actuellement : " + k);

    }

    /**
     * Acceseur de l'attribut monde
     *
     * @return l'attribut monde du controleur (classe Monde)
     */
    private Monde getMonde() {
        return this.monde;
    }

    /**
     * Mutateur de l'attribut scene du controlleur
     *
     * @param scene nouvelle scene
     */
    public void setScene(Scene scene) {
        this.scene = scene;
    }

    /**
     * Methode appelee par le bouton Nouvelle Partie definit dans le sample qui permet de changer du panneau initial au panneau de definition du
     * nombre de joueur
     *
     * @param actionEvent Tout clic sur le bouton Nouvelle Partie va lancer la fonction
     */
    public void newPartie(ActionEvent actionEvent) {
        this.Debut.setVisible(false);
        ComboBox<Integer> box = new ComboBox<>();
        box.setLayoutX(1100);
        box.setLayoutY(14.0);
        for (int k = 2; k <= 6; k++) {
            box.getItems().add(k);
        }
        box.valueProperty().addListener(observable -> setNombreJoueur(box.getValue()));
        this.nombreJoueur.getChildren().add(box);
        this.nombreJoueur.setVisible(true);
        this.nombreJoueur.toFront();
    }

    /**
     * Methode appelle par le bouton Valider du panneau nombreJoueur et qui va valider le nombre de joueur actuellement selectionne
     *
     * @param actionEvent Tout clic sur le bouton Valider du panneau nombreJoueur va lancer la fonction
     */
    public void valider(ActionEvent actionEvent) {
        if (this.numJoueur != 0) {
            this.nombreJoueur.setVisible(false);
            this.creationJoueur.setVisible(true);
            this.textNombreJoueur.setText("Nombre de joueur a faire : " + this.numJoueur);
            List<String> couleursList = new ArrayList<>();
            couleursList.add("Bleu");
            couleursList.add("Jaune");
            couleursList.add("Orange");
            couleursList.add("Rose");
            couleursList.add("Rouge");
            couleursList.add("Turquoise");
            couleursList.add("Vert");
            couleursList.add("Violet");
            for (String couleur : couleursList) {
                this.couleurs.getItems().add(couleur);
            }
            this.couleurs.valueProperty().addListener(observable -> setColorJoueur(this.couleurs.getValue()));
        }

    }

    /**
     * Mutateur de l'attribut couleurJoueur. Elle va en plus changer la tete afficher par teteJoueur pour etre en accord avec la couleur
     * selectionnee
     *
     * @param couleur String qui va definir le nouvel attribut couleurJoueur
     */
    private void setColorJoueur(String couleur) {
        this.couleurJoueur = couleur;
        this.teteJoueur.setImage(new Image("file:./graphisme/Perso/Perso" + this.couleurJoueur + ".png", 200, 200, true, true));
    }

    /**
     * Methode de validation des joueurs et de la nouvelle partie creer. Elle va aussi changer de controlleur pour passer au jeu de plateau
     *
     * @param actionEvent Tout clic sur le bouton Valider du panneau creationJoueur va lancer la fonction
     * @throws IOException le fichier jeu.fxml est normalement definit
     */
    public void ValdierJoueur(ActionEvent actionEvent) throws IOException {
        if (this.numJoueur > 1) {
            if (this.couleurs.getValue() == null || this.monde.getNomJoueurs().contains(this.nom.getCharacters().toString())) {
            } else {
                this.numJoueur -= 1;
                this.textNombreJoueur.setText("Nombre de joueur a faire : " + this.numJoueur);
                Joueur joueur = new Joueur(this.nom.getCharacters().toString(), this.couleurJoueur, new ArrayList<Province>(), new Reserve(),
                        new Deck(), new ArrayList<Continent>());
                joueur.getConscription().setJoueur(joueur);
                this.monde.getJoueurs().add(joueur);
                this.couleurs.getItems().remove(this.couleurJoueur);
                this.nom.clear();
            }

        } else {
            Joueur joueur = new Joueur(this.nom.getCharacters().toString(), this.couleurJoueur, new ArrayList<Province>(), new Reserve(),
                    new Deck(), new ArrayList<Continent>());
            joueur.getConscription().setJoueur(joueur);
            this.monde.getJoueurs().add(joueur);
            this.creationJoueur.setVisible(false);
            this.monde.nouvellePartie();
            this.monde.setJoueurTour(0);
            this.monde.getJoueurs().get(0).getConscription().recruter();
            Mondefinal = this.getMonde();
            this.scene.setRoot(FXMLLoader.load((getClass().getResource("jeu.fxml"))));
        }
    }

    /**
     * Methode appelle par le bouton Charger Partie qui va permettre de passer du panneau initial au panneau de chargement de partie
     *
     * @param actionEvent Tout clic sur le bouton Charger Partie va lancer la fonction
     */
    public void loadPartie(ActionEvent actionEvent) {
        this.Debut.setVisible(false);
        this.chargement.setVisible(true);
        File repertoire = new File(".\\sauvegardes");
        File[] files = repertoire.listFiles();
        if (files != null) {
            for (File file : files) {
                this.sauvegardes.getItems().add(file.getName());
            }
        }
    }

    /**
     * Methode de validation de la partie chargee. Elle va aussi changer de controlleur pour passer au jeu de plateau
     *
     * @param actionEvent Tout clic sur le bouton Valider du panneau chargement va lancer la fonction
     * @throws IOException le fichier jeu.fxml est normalement definit
     */
    public void ChargerPartie(ActionEvent actionEvent) throws IOException {
        if (this.sauvegardes.getValue() != null) {
            this.monde.setJoueurTour(this.monde.chargerPartie(this.sauvegardes.getValue()));
            Mondefinal = this.getMonde();
            this.scene.setRoot(FXMLLoader.load((getClass().getResource("jeu.fxml"))));
        }
    }

    /**
     * Methode appeler par le bouton retour present sur differents panneau et permettant de revenoir au menu principal en reinitialisant
     * differentes valeurs du controleur (numJoueur, Joueurs du monde, nomJoueur du monde, couleurs, sauvegardes)
     *
     * @param actionEvent Tout clic sur le bouton Retour va lancer la fonction
     */
    public void RetourEcranTitre(ActionEvent actionEvent) {
        this.creationJoueur.setVisible(false);
        this.Debut.setVisible(true);
        this.monde.getJoueurs().clear();
        this.monde.getNomJoueurs().clear();
        this.couleurs.getItems().clear();
        this.indicNum.setText("Nombre de joueurs actuellement : aucun");
        this.numJoueur = 0;
        this.nombreJoueur.setVisible(false);
        this.chargement.setVisible(false);
        this.sauvegardes.getItems().clear();
        this.teteJoueur.setImage(new Image("file:./graphisme/Perso/Perso.png", 200, 200, true, true));
    }
}



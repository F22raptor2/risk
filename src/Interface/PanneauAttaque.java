package Interface;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * Classe decrivant les panneaux d'attaque
 *
 * @author AsTeRisk
 */

public class PanneauAttaque extends JPanel {

    /**
     * L'adresse du dossier contenant toutes les illustrations utilisees dans le jeu
     */
    private String chemin;
    /**
     * La liste des resultats des des d'attaque du lancer en cours
     */
    private List<Integer> desAttaque;
    /**
     * La liste des resultats des des de defense du lancer en cours
     */
    private List<Integer> desDefense;
    /**
     * Un entier qui, en variant, anime le panneau
     */
    private int j;
    /**
     * S'il est temps ou non de reveler l'issue de l'attaque
     */
    private boolean spoil;
    /**
     * Si l'attaque a reussi ou pas
     */
    private boolean reussite;

    /**
     * Mutateur de j
     *
     * @param j Parametre de position et d'affichage
     */
    public void setJ(int j) {
        this.j = j;
    }

    /**
     * Mutateur de la liste des resultats des des d'attaque
     *
     * @param desAttaque La liste des resultats des des d'attaque du nouveau lancer
     */
    public void setDesAttaque(List<Integer> desAttaque) {
        this.desAttaque = desAttaque;
    }

    /**
     * Mutateur de la liste des resultats des des de defense
     *
     * @param desDefense La liste des resultats des des de defense du nouveau lancer
     */
    public void setDesDefense(List<Integer> desDefense) {
        this.desDefense = desDefense;
    }

    /**
     * Mutateur du chemin
     *
     * @param chemin Le chemin vers le dossier contenant les illustrations du jeu
     */
    public void setChemin(String chemin) {
        this.chemin = chemin;
    }

    /**
     * Mutateur du spoiler
     *
     * @param b Si on affiche le resultat de l'attaque, ou pas encore
     */
    public void setSpoil(boolean b) {
        this.spoil = b;
    }

    /**
     * Mutateur de la reussite de l'attaque
     *
     * @param reussite Si l'attaque est victorieuse
     */
    public void setReussite(boolean reussite) {
        this.reussite = reussite;
    }

    /**
     * Methode appelee automatiquement, qui dessine les elements que l'on souhaite sur le panneau
     */
    public void paintComponent(Graphics g) {

        if (!spoil) {        // soit tant qu'on n'a pas fini l'animation
            this.setLayout(null); //Pour pouvoir placer les elements ou on veut

            // On dessine un rectangle brun pour le fond
            g.setColor(new Color(55, 46, 40));
            g.fillRect(0, 0, this.getWidth(), this.getHeight());

            //On ajoute une image de fond
            try {
                Image bataille = ImageIO.read(new File(chemin + "\\bataille.jpg"));
                g.drawImage(bataille, 0, (Math.abs(this.getHeight() - this.getWidth() * 48 / 90) / 2), this.getWidth(), this.getWidth() * 482 / 900, this);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // On ajoute l'image d'un eclair, qui apparaitra par intermittence - quand 150 <= j<= 199 ici, puisque j s'arrete a 250
            try {
                Image eclair = ImageIO.read(new File(chemin + "\\eclair.png"));
                if (j / 50 == 3) g.drawImage(eclair, 0, 0, this.getWidth(), this.getHeight(), this);
            } catch (IOException e) {
                e.printStackTrace();
            }

            int nbDesA = this.desAttaque.size();
            // On dessine les des d'attaque
            for (int i = 0; i < nbDesA; i++) {
                int x = j / 2 * ((i + 1) / 2 + 2 * ((i + 1) % 2));    // reglage de la position horizontale dependant du de et de j
                int y = ((2 * i + 1) * this.getHeight()) / (2 * nbDesA);    // reglage de la position verticale
                g.setColor(new Color(240, 186, 181));    // un rouge clair
                g.fill3DRect(x, y, 60, 60, true);
                g.setColor(new Color(200, 50, 50));    // un rouge plus sombre

                // on dessine les points des des
                if (desAttaque.get(i) % 2 == 1) {
                    g.fillOval(x + 25, y + 25, 10, 10);
                }
                if (desAttaque.get(i) >= 2 && desAttaque.get(i) <= 5) {
                    g.fillOval(x + 10, y + 10, 10, 10);
                    g.fillOval(x + 40, y + 40, 10, 10);
                }
                if (desAttaque.get(i) >= 4 && desAttaque.get(i) <= 5) {
                    g.fillOval(x + 10, y + 40, 10, 10);
                    g.fillOval(x + 40, y + 10, 10, 10);
                }
                if (desAttaque.get(i) == 6) {
                    g.fillOval(x + 12, y + 10, 10, 10);
                    g.fillOval(x + 12, y + 25, 10, 10);
                    g.fillOval(x + 12, y + 40, 10, 10);
                    g.fillOval(x + 38, y + 10, 10, 10);
                    g.fillOval(x + 38, y + 25, 10, 10);
                    g.fillOval(x + 38, y + 40, 10, 10);
                }
            }
            int nbDesD = this.desDefense.size();
            //On dessine les des de defense
            for (int i = 0; i < nbDesD; i++) {
                int x = this.getWidth() - 60 - j / 2 * (i + 1);    // reglage de la position horizontale, qui depend du de et de j
                int y = ((2 * i + 1) * this.getHeight()) / (2 * nbDesD);    // reglage de la position verticale
                g.setColor(new Color(192, 186, 240));    // un bleu clair
                g.fill3DRect(x, y, 60, 60, true);
                g.setColor(new Color(50, 50, 200));    // un bleu plus sombre

                //On dessine les points sur les des
                if (desDefense.get(i) % 2 == 1) {
                    g.fillOval(x + 25, y + 25, 10, 10);
                }
                if (desDefense.get(i) >= 2 && desDefense.get(i) <= 5) {
                    g.fillOval(x + 10, y + 10, 10, 10);
                    g.fillOval(x + 40, y + 40, 10, 10);
                }
                if (desDefense.get(i) >= 4 && desDefense.get(i) <= 5) {
                    g.fillOval(x + 10, y + 40, 10, 10);
                    g.fillOval(x + 40, y + 10, 10, 10);
                }
                if (desDefense.get(i) == 6) {
                    g.fillOval(x + 12, y + 10, 10, 10);
                    g.fillOval(x + 12, y + 25, 10, 10);
                    g.fillOval(x + 12, y + 40, 10, 10);
                    g.fillOval(x + 38, y + 10, 10, 10);
                    g.fillOval(x + 38, y + 25, 10, 10);
                    g.fillOval(x + 38, y + 40, 10, 10);
                }
            }
        } else { //spoil vaut true, donc on est sortis de la phase d'animation

            // Un rectangle clair pour le fond
            g.setColor(new Color(192, 186, 181));
            g.fillRect(0, 0, this.getWidth(), this.getHeight());

            // On regle la police d'ecriture et la couleur
            g.setColor(new Color(0, 60, 10));    // un vert sombre
            Font font = new Font("Tahoma", Font.BOLD, 40);
            g.setFont(font);

            // En cas de reussite de l'attaque, on affiche les lauriers
            if (reussite) {
                try {
                    Image lauriers = ImageIO.read(new File(chemin + "\\lauriers.png"));
                    if (this.getWidth() * 13 / 15 < this.getHeight()) {
                        g.drawImage(lauriers, 0, (this.getHeight() - this.getWidth() * 13 / 15) / 2,
                                this.getWidth(), (this.getWidth() * 13) / 15, this);
                    } else g.drawImage(lauriers, (this.getWidth() - this.getHeight() * 15 / 13) / 2, 0,
                            this.getHeight() * 15 / 13, this.getHeight(), this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                g.drawString("Conquête victorieuse", (Math.abs(this.getWidth() - 480)) / 2, this.getHeight() / 2);
            }

            // Sinon on affiche juste que la defense a resiste
            else g.drawString("La défense résista!", (Math.abs(this.getWidth() - 460)) / 2, this.getHeight() / 2);
        }


    }

}

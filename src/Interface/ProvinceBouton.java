package Interface;

import javafx.scene.control.Button;
import Modele.Province;

/**
 * Classe qui represente un bouton associe a une province
 *
 * @author AsTeRisk
 */
public class ProvinceBouton extends Button {

    /**
     * Province associe au bouton
     */
    private Province province;

    /**
     * Constructeur du bouton a partir de sa province
     *
     * @param province province qui est associe au bouton
     */
    public ProvinceBouton(Province province) {
        super();
        this.province = province;
        this.setPrefHeight(30);
        this.setPrefWidth(100);
        this.setOpacity(0.2);
        this.setVisible(true);

    }

    /**
     * Acceusseur de l'attribut province
     *
     * @return
     */
    public Province getProvince() {
        return this.province;
    }

}

package Modele;

/**
 * Classe qui definit les cartes Bonus
 *
 * @author AsTeRisk
 */

public class Bonus {

    /**
     * Nom de province affiche sur la carte, plus pour la deco qu'autre chose
     */
    private String nomProvince;
    /**
     * Categorie du bataillon de la carte
     */
    private TypeBataillon categorieBataillon;
    /**
     * Identifiant de la carte, servant a rendre la sauvegarde plus aisee
     */
    private int Ident;

    /**
     * Constructeur de carte bonus
     *
     * @param Ident              son identifiant
     * @param nomProvince        le nom de province de cette carte
     * @param categorieBataillon sa categorie de bataillon
     */
    public Bonus(int Ident, String nomProvince, TypeBataillon categorieBataillon) {
        this.Ident = Ident;
        this.nomProvince = nomProvince;
        this.categorieBataillon = categorieBataillon;
    }

    /**
     * Accesseur du nom de province
     *
     * @return le nom de la province
     */
    public String getNomProvince() {
        return nomProvince;
    }

    /**
     * Accesseur de la categorie de bataillon
     *
     * @return la categorie de bataillon
     */
    public TypeBataillon getCategorieBataillon() {
        return categorieBataillon;
    }

    /**
     * Accesseur de l'identifiant
     *
     * @return l'identifiant
     */
    public int getIdent() {
        return Ident;
    }

    /**
     * Mutateur de l'identifiant
     *
     * @param ident le nouvel identifiant
     */
    public void setIdent(int ident) {
        Ident = ident;
    }


}

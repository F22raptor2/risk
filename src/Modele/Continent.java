package Modele;

import java.util.List;

/**
 * Classe decrivant les proprietes des continents
 *
 * @author AsTeRisk
 */
public class Continent {

    /**
     * Le nom du continent
     */
    private String nom;
    /**
     * La liste des provinces composant le continent
     */
    private List<Province> provinces;
    /**
     * Le nombre de renforts supplementaires recus par le joueur possedant le continent entier
     */
    private int BonusRenfort;


    /**
     * Generateur de continent
     *
     * @param nom          Son nom
     * @param provinces    Ses provinces
     * @param bonusRenfort Le nombre de renforts en bonus
     */
    public Continent(String nom, List<Province> provinces, int bonusRenfort) {
        super();
        this.nom = nom;
        this.provinces = provinces;
        this.BonusRenfort = bonusRenfort;
    }


    /**
     * Accesseur du nom
     *
     * @return son nom
     */
    public String getNom() {
        return this.nom;
    }


    /**
     * Accesseur du nombre de bonus en renforts
     *
     * @return le nombre de renforts qu'il apporte
     */
    public int getBonusRenfort() {
        return this.BonusRenfort;
    }

    /**
     * Accesseur des provinces
     *
     * @return la liste de ses provinces
     */
    public List<Province> getProvinces() {
        return this.provinces;
    }


}

package Modele;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

/**
 * Classe decrivant la main d'un joueur
 *
 * @author AsTeRisk
 */

public class Deck {

    /**
     * La liste des cartes evenement possedees par le joueur
     */
    private List<Evenement> mainEvenement;
    /**
     * La liste des cartes bonus possedees par le joueur
     */
    private List<Bonus> mainBonus;

    /**
     * Constructeur de la main d'un joueur
     */
    public Deck() {
        super();
        this.mainEvenement = new ArrayList<Evenement>();
        this.mainBonus = new ArrayList<Bonus>();
    }

    /**
     * Accesseur des cartes evenement
     *
     * @return la liste des cartes evenement
     */
    public List<Evenement> getMainEvenement() {
        return mainEvenement;
    }

    /**
     * Mutateur des cartes evenement
     *
     * @param mainEvenement la liste des nouvelles cartes evenement
     */
    public void setMainEvenement(List<Evenement> mainEvenement) {
        this.mainEvenement = mainEvenement;
    }

    /**
     * Accesseur des cartes bonus
     *
     * @return la liste des cartes bonus
     */
    public List<Bonus> getMainBonus() {
        return mainBonus;
    }

    /**
     * Mutateur des cartes bonus
     *
     * @param mainEvenement la liste des nouvelles cartes bonus
     */
    public void setMainBonus(List<Bonus> mainBonus) {
        this.mainBonus = mainBonus;
    }

    /**
     * Methode permettant de calculer les bataillons que le joueur pourrait obtenir en jouant
     * une combinaison de cartes de sa main
     *
     * @return le dictionnaire ayant pour cles les nombres de bataillons que le joueur peut gagner,
     * et pour valeurs les combinaisons de cartes bonus correspondantes.
     */
    public Map<Integer, List<Bonus>> calculerNouveauBataillon() {
        HashMap<Integer, List<Bonus>> res = new HashMap<Integer, List<Bonus>>();
        HashMap<TypeBataillon, List<Bonus>> table = new HashMap<TypeBataillon, List<Bonus>>();

        for (Bonus b : this.mainBonus) {
            TypeBataillon categ = b.getCategorieBataillon();
            if (table.containsKey(categ)) {
                table.get(categ).add(b);
            } else {
                List<Bonus> lili = new ArrayList<Bonus>();
                lili.add(b);
                table.put(categ, lili);
            }
        }
        for (TypeBataillon toto : table.keySet()) {
            if (table.get(toto).size() >= 3) {
                switch (toto) {
                    //On limite à 3 cartes de ce type
                    case TroupierDeBase:
                        res.put(1, table.get(toto).subList(0, 3));
                        break;
                    case CanonFarceur:
                        res.put(5, table.get(toto).subList(0, 3));
                        break;
                    case CavalierSuicidaire:
                        res.put(3, table.get(toto).subList(0, 3));
                }
            }
        }
        if (table.size() >= 3) {
            List<Bonus> lulu = new ArrayList<Bonus>();
            for (TypeBataillon b : table.keySet()) {
                lulu.add(table.get(b).get(0));
            }
            res.put(4, lulu);
        }
        return res;
    }

    /**
     * Methode permettant de gerer la pioche d'une carte, en prenant en compte qu'un
     * joueur peut avoir au maximum 5 cartes bonus et 5 cartes evenement
     *
     * @param pioche La pioche de la partie en cours
     */
    public void piocher(Pioche pioche) { //On pioche aleatoirement dans la pioche
        Random plouf = new Random();

        // choisit aleatoirement la carte a piocher
        Bonus piocheee = pioche.getPonussss().get(plouf.nextInt(pioche.getPonussss().size()));
        this.mainBonus.add(piocheee);
        // met a jour la pioche
        pioche.getPonussss().remove(piocheee);


    }


}
package Modele;

/**
 * Classe non encore implementee decrivant les cartes evenement
 *
 * @author AsTeRisk
 */
public class Evenement {

    /**
     * L'identifiant de la carte
     */
    private int Ident;

    /**
     * Accesseur de l'identifiant
     *
     * @return l'identifiant de la carte
     */
    public int getIdent() {
        return this.Ident;
    }

    /**
     * Mutateur de l'identifiant
     *
     * @param ident L'identifiant de la carte
     */
    public void setIdent(int ident) {
        this.Ident = ident;
    }

}


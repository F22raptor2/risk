package Modele;

import java.util.List;
import java.util.Scanner;

/**
 * Classe decrivant les caracteristiques et actions des joueurs
 *
 * @author AsTeRiskpackage
 */

public class Joueur {
    /**
     * Son nom
     */
    private String nom;
    /**
     * Sa couleur
     */
    private String couleur;
    /**
     * Sa reserve d'effectifs
     */
    private Reserve conscription;
    /**
     * Les cartes de sa main
     */
    private Deck deck;
    /**
     * La liste des provinces qu'il possede
     */
    private List<Province> empire;
    /**
     * La liste des continents qu'il possede
     */
    private List<Continent> pangee;


    /**
     * Generateur de joueur
     *
     * @param nom          Son nom
     * @param couleur      Sa couleur
     * @param empire       Le liste de ses provinces
     * @param conscription Son armee de reserve
     * @param deck         Les cartes de sa main
     */
    public Joueur(String nom, String couleur, List<Province> empire, Reserve conscription, Deck deck, List<Continent> pangee) {
        super();
        this.nom = nom;
        this.couleur = couleur;
        this.empire = empire;
        this.conscription = conscription;
        this.deck = deck;
        this.pangee = pangee;

    }

    /**
     * Generateur de joueur
     *
     * @param nom     Son nom
     * @param couleur Sa couleur
     */
    public Joueur(String nom, String couleur) {
        this.nom = nom;
        this.couleur = couleur;
    }

    /**
     * Accesseur des continents que possede le joueur
     *
     * @return la liste des continents que possede le joueur
     */
    public List<Continent> getPangee() {
        return this.pangee;
    }

    /**
     * Accesseur du nom
     *
     * @return le nom du joueur
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Mutateur du nom
     *
     * @param nomJoueur Le nouveau nom du joueur
     */
    public void setNom(String nomJoueur) {
        this.nom = nomJoueur;
    }

    /**
     * Accesseur de la couleur
     *
     * @return la couleur du joueur
     */
    public String getCouleur() {
        return this.couleur;
    }

    /**
     * Mutateur de la couleur
     *
     * @param couleurJoueur La nouvelle couleur du joueur
     */
    public void setColor(String couleurJoueur) {
        this.couleur = couleurJoueur;

    }

    /**
     * Accesseur de l'empire
     *
     * @return la liste des provinces possedees par le joueur
     */
    public List<Province> getEmpire() {
        return this.empire;
    }

    /**
     * Mutateur de l'empire
     *
     * @param conquete La nouvelle province a ajouter a l'empire
     */
    public void setEmpire(Province conquete) {
        this.empire.add(conquete);
    }

    /**
     * Accesseur de la conscription
     *
     * @return la conscription du joueur
     */
    public Reserve getConscription() {
        return this.conscription;
    }

    /**
     * Accesseur de la main
     *
     * @return la main du joueur
     */
    public Deck getDeck() {
        return this.deck;
    }


    /**
     * Methode permettant de retrouver une province appartenant a ce joueur a partir de son nom
     *
     * @param nom Le nom de la province
     * @return cette province si elle fait bien partie de l'empire du joueur, null sinon
     */
    public Province getProvince(String nom) {
        for (Province p : this.empire) {
            if (p.getNom().equals(nom)) return p;
        }
        System.out.println("cette province ne t'appartient pas");
        return null;
    }

    /**
     * Methode affichant dans la console les caracteristiques de l'empire du joueur
     */
    public void empireToStringe() {
        for (int i = 0; i < this.empire.size(); i++) {
            System.out.println((i + "- " + empire.get(i).getNom()) + " - regiments : " + empire.get(i).getRegiments() +
                    " - regiments fatiguÃ©s : " + empire.get(i).getRegimentsFatigues());
        }
    }

    /**
     * Methode controlant le placement de regiments depuis la reserve dans une province
     *
     * @param regiments Le nombre de troupes a placer
     * @param cible     La province ou les placer
     */
    public void placerRenfort(int regiments, Province cible) {
        if (this.conscription.getCaserne() < regiments) {
            System.out.println("Tu as louchÃ©");
            return;
        }
        if (!this.empire.contains(cible)) {
            System.out.println("Bon sang CA N'EST PAS CHEZ TOI !! - au pire attaque-les si tu tiens tant Ã  mettre tes troupes lÃ  -");
            return;
        }
        this.conscription.viderCaserne(regiments);
        cible.setRegiments(cible.getRegiments() + regiments);
    }

    /**
     * Methode permettant de placer des renforts dans differentes provinces, utilisee dans la version lignes de
     * commande du jeu
     *
     * @param choix Le scanner
     */
    public void placerRenfortsJoueur(Scanner choix) {

        this.empireToStringe();
        System.out.println("Voici ton nombre de rÃ©giments disponibles : " + this.conscription.getCaserne());
        System.out.println("saisissez  \"placer\" , le nombre de renforts et une province (avec son nom) - \"stop\" pour arrÃªter le placement");

        //while (choix.next() != "stop") {
        while (this.getConscription().getCaserne() > 0) {
            String actionRenfort = choix.next();
            if (actionRenfort.equals("stop")) {
                break;
            }
            String nbRenfort = choix.next();
            //choix.nextLine();
            String nomProvince = choix.next();
            //choix.nextLine();
            System.out.println(nbRenfort + " - " + nomProvince);
            // IL FAUT GERER LES EXCEPTIONS - car les joueurs sont de sacres cons -
            placerRenfort(Integer.parseInt(nbRenfort), getProvince(nomProvince));
            this.empireToStringe();
            System.out.println("Voici ton nombre de rÃ©giments disponibles : " + this.conscription.getCaserne());
            //System.out.println("nombre de renforts en caserne: " + this.conscription.getCaserne() + "\n");
        }
    }

    /**
     * Methode revigorant les regiments du joueur
     */
    public void finDeTour() {
        for (Province p : this.empire) {
            p.setRegimentsFatigues(0);
        }
        System.out.println("fin de tour - tu trouves vraAaiment que tu as bien jouÃ©? - ");
    }

}
package Modele;

import Sauvegarde.Docks;
import Sauvegarde.Save;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Classe qui represete un Monde une fois une partie debutee
 *
 * @author AsTeRisk
 */

public class Monde {

    /**
     * Liste des joueurs presents dans la partie
     */
    private List<Joueur> joueurs;
    /**
     * Liste des continents du monde
     */
    private List<Continent> continents;
    /**
     * Liste des provinces presentes dans le monde
     */
    private List<Province> provinces;
    /**
     * Pioche du monde de jeu
     */
    private Pioche pioche;
    /**
     * Identifiant dans la liste des joueurs du joueurs dont c'est le tout (utilise lors du chargement du monde)
     */
    private int joueurTour;
    /**
     * Liste des differents noms des joueurs pour eviter que deux joueurs ai le meme nom
     */
    private List<String> nomJoueurs;
    /**
     * List de coordonnees (X,Y) pour le positionnement des regiments sur les provinces par rapport au debut de la province
     */
    private List<List<Integer>> layoutProvince;

    /**
     * Constructeur d'un Monde de jeu vide
     */
    public Monde() {
        ///Creation du monde///

        Docks docks = new Docks();
        //Creation des provinces
        List<String> fileProvince = docks.loadFile(".\\monde\\Provinces");
        fileProvince.remove(0);
        this.provinces = docks.createProvince(fileProvince);
        //Creation des continents
        List<String> fileContinent = docks.loadFile(".\\monde\\Continents");
        fileContinent.remove(0);
        this.continents = docks.createContinents(fileContinent, provinces);
        //Ajout des contients aux provinces
        docks.ajouteContinentAProvinces(this.provinces, this.continents);
        //Creation de la liste de cartes Bonus
        List<String> fichier = docks.loadFile(".\\monde\\Bonus");
        List<Bonus> listeCartes = docks.creePiscineABonus(fichier);
        this.layoutProvince = new ArrayList<>();
        ArrayList<Integer> positionA = new ArrayList<>();
        positionA.add(10);
        positionA.add(-13);
        layoutProvince.add(positionA);
        ArrayList<Integer> positionB = new ArrayList<>();
        positionB.add(46);
        positionB.add(-7);
        layoutProvince.add(positionB);
        ArrayList<Integer> positionC = new ArrayList<>();
        positionC.add(67);
        positionC.add(6);
        layoutProvince.add(positionC);
        ArrayList<Integer> positionD = new ArrayList<>();
        positionD.add(63);
        positionD.add(15);
        layoutProvince.add(positionD);
        ArrayList<Integer> positionE = new ArrayList<>();
        positionE.add(43);
        positionE.add(28);
        layoutProvince.add(positionE);
        ArrayList<Integer> positionF = new ArrayList<>();
        positionF.add(15);
        positionF.add(25);
        layoutProvince.add(positionF);
        ArrayList<Integer> positionG = new ArrayList<>();
        positionG.add(-15);
        positionG.add(18);
        layoutProvince.add(positionG);
        ArrayList<Integer> positionH = new ArrayList<>();
        positionH.add(-7);
        positionH.add(9);
        layoutProvince.add(positionH);

        ///initialisation des listes de joueurs et de la pioche///
        this.joueurs = new ArrayList<>();
        this.nomJoueurs = new ArrayList<>();
        this.pioche = new Pioche(new ArrayList<Bonus>());
        pioche.getPonussss().addAll(listeCartes);
        this.joueurTour = 0;
    }

    /**
     * Accesseur de la liste de joueurs
     *
     * @return liste des joueurs du monde
     */
    public List<Joueur> getJoueurs() {
        return joueurs;
    }

    /**
     * Accesseur de la liste des noms des joueurs
     *
     * @return
     */
    public List<String> getNomJoueurs() {
        return nomJoueurs;
    }

    /**
     * Accesseur de la liste des provinces du monde
     *
     * @return
     */
    public List<Province> getProvinces() {
        return this.provinces;
    }

    /**
     * Mutateur de l'attribut joueurTour
     *
     * @param joueurTour nouvelle valuer de l'attribut
     */
    public void setJoueurTour(int joueurTour) {
        this.joueurTour = joueurTour;
    }

    /**
     * Accesseur de l'attribut joueurTour
     *
     * @return valuer de l'attribut
     */
    public int getJoueurTour() {
        return this.joueurTour;
    }

    /**
     * Accesseur de l'attribut layoutProvince
     *
     * @return valeur de l'attribut
     */
    public List<List<Integer>> getLayoutProvince() {
        return this.layoutProvince;
    }

    /**
     * Acceseur de la liste des continents
     *
     * @return liste des continents du monde
     */
    public List<Continent> getContinents() {
        return this.continents;
    }

    /**
     * Accesseur de la pioche
     *
     * @return pioche du monde
     */
    public Pioche getPioche() {
        return this.pioche;
    }


    /**
     * Methode qui permet de lancer une nouvelle partie. Elle va entre autre ajouter des provinces a tous les joueurs presents.
     */
    public void nouvellePartie() {
        //Ajout de provinces aleatoirement pour chaque joueur
        List<Province> provincesAAtribuees = new ArrayList<>();
        provincesAAtribuees.addAll(this.provinces);
        int provincesAttribuees = 0;
        int nbProvinces = this.provinces.size();
        Random random = new Random();
        while (provincesAttribuees < nbProvinces) {
            for (Joueur j : this.joueurs) {
                int idProvince = random.nextInt(provincesAAtribuees.size());
                j.getEmpire().add(provincesAAtribuees.get(idProvince));
                provincesAAtribuees.get(idProvince).setGouvernant(j);
                provincesAAtribuees.get(idProvince).setRegiments(2);
                provincesAAtribuees.get(idProvince).setRegimentsFatigues(0);
                provincesAAtribuees.remove(idProvince);
                provincesAttribuees++;
                if (provincesAttribuees == nbProvinces) {
                    break;
                }
            }
        }

    }

    /**
     * Methode qui permet de charger une partie a partir du nom de la sauvegarde
     *
     * @param name nom de la suavegarde
     * @return l'identifiant du joueur dot c'est le tour
     */
    public int chargerPartie(String name) {
        Save sauvegarde = new Save();
        Docks dock = new Docks();
        String filePathJoueur = ".\\sauvegardes\\" + name + "\\saveJoueur";
        String filePathPioche = ".\\sauvegardes\\" + name + "\\savePioche";
        this.joueurTour = sauvegarde.loadJoueur(dock.loadFile(filePathJoueur), this.provinces, this.pioche.getPonussss(), this.continents, this.joueurs);
        sauvegarde.loadPioche(this.pioche.getPonussss(), dock.loadFile(filePathPioche), this.pioche);
        return joueurTour;
    }

    /**
     * Methode qui permet de suavegarder l'etat de la partie dans deux fichiers (saveJoueurs et savePioche) contenant les informations
     * sur les joueurs (leurs provinces, leur main,...) et sur la pioche (cartes presnetes dans celle-ci)
     *
     * @param name nom que l'on veut donner a la nouvelle sauvegarde
     */
    public void sauvegardePartie(String name) {
        Save sauvegarde = new Save();
        String dirName = ".\\sauvegardes\\" + name;
        File dir = new File(dirName);
        dir.mkdirs();
        String filePathJoueur = ".\\sauvegardes\\" + name + "\\saveJoueur";
        String filePathPioche = ".\\sauvegardes\\" + name + "\\savePioche";
        sauvegarde.saveJoueur(filePathJoueur, joueurs, joueurTour);
        sauvegarde.savePioche(filePathPioche, pioche);
    }

    /**
     * Methode qui permet de verifier si un joueur a gagne la partie ou non
     *
     * @return true si la partie est finie(ie un joueur possede tout le monde) et false sinon
     */
    public boolean finDeJeu() {
        for (int i = 0; i < this.joueurs.size(); i++) {
            if (this.joueurs.get(i).getEmpire().size() == this.provinces.size()) {
                System.out.println("Le joueur " + this.joueurs.get(i).getNom() + " a exterminé la vermine qui l'entourait! \n Bravo à lui c'est un exploit remarquable\n " +
                        "maintenant il n'a plus qu'à retourner à sa misérable petite vie dans la réalité réelle de la vérité véritable.");
                return true;
            }
        }
        return false;
    }
}

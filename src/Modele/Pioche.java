package Modele;

import Sauvegarde.Docks;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe decrivant la pioche
 *
 * @author AsTeRisk
 */

public class Pioche {
    /**
     * Liste des cartes evenement contenues dans la pioche
     */
    private List<Evenement> evenements;
    /**
     * Liste des cartes bonus contenues dans la pioche
     */
    private List<Bonus> ponussss;

    /**
     * Generateur de la pioche
     *
     * @param ponussss La liste des cartes bonus
     */
    public Pioche(List<Bonus> ponussss) {
        super();
        //this.evenements = evenements;
        this.ponussss = ponussss;
    }

    /**
     * Accesseur des cartes evenement
     *
     * @return la liste des cartes evenement dans la pioche
     */
    public List<Evenement> getEvenements() {
        return evenements;
    }

    /**
     * Mutateur de la liste de cartes evenement
     *
     * @param evenements Les nouvelles cartes evenement
     */
    public void setEvenements(List<Evenement> evenements) {
        this.evenements = evenements;
    }

    /**
     * Accesseur des cartes bonus
     *
     * @return la liste des cartes bonus dans la pioche
     */
    public List<Bonus> getPonussss() {
        return ponussss;
    }

    /**
     * Mutateur de la liste de cartes bonus
     *
     * @param ponussss Les nouvelles cartes bonus
     */
    public void setPonussss(List<Bonus> ponussss) {
        this.ponussss = ponussss;
    }


    // Cette fonction est pour l'instant inutile puisqu'on n'a pas encore cree de cartes evenement
    /*
     * public void CreateEventPool(List<String> file) {

    	List<Evenement> events = new ArrayList<>();
    	for (String s :file) {
    		String[] donneesEvent = s.split(" ");

    		 events.add(new Evenement());

    	}
    }
    */

    /**
     * Methode permettant de remplir la pioche lorsqu'on recharge une partie
     *
     * @param fichier       La liste de lignes du fichier dans lequel les carte bonus ont ete enregistrees
     * @param donneesPioche La liste des identifiants des cartes bonus qui sont dans la pioche
     */
    public void loadPioche(List<String> fichier, List<String> donneesPioche) {
        List<Bonus> bonus = new ArrayList<>();
        Docks docks = new Docks();
        bonus = docks.creePiscineABonus(fichier);
        List<Bonus> bonusPioche = new ArrayList<>();
        String[] donnees = donneesPioche.get(0).split(";");
        for (String s : donnees) {
            bonusPioche.add(bonus.get(Integer.valueOf(s) - 1));
        }
        this.ponussss = bonusPioche;
    }

    /**
     * Methode permettant d'enregistrer les cartes bonus qui sont encore dans la pioche au moment
     * ou on sauvegarde la partie
     *
     * @param filePath L'adresse du fichier ou on va l' enregistrer
     */
    public void savePioche(String filePath) {
        Path logFile = Paths.get(filePath);
        if (!Files.exists(logFile)) {
            try {
                Files.createFile(logFile);
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
        try (BufferedWriter writer = Files.newBufferedWriter(logFile,
                StandardCharsets.UTF_8, StandardOpenOption.WRITE)) {
            String line = "";
            for (Bonus b : this.ponussss) {
                line += b.getIdent() + ";";
            }
            writer.write(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


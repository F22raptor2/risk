package Modele;

import Interface.FenetreAttaque;

import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * Classe decrivant les provinces
 *
 * @author AsTeRisk
 */
public class Province {

    /**
     * Son nom
     */
    private String nom;
    /**
     * Le continent auquel elle appartient
     */
    private Continent continent;
    /**
     * Son identifiant
     */
    private int ident;
    /**
     * La presence d'un fort dessus
     */
    private boolean fort;
    /**
     * Les provinces voisines
     */
    private List<Province> voisins;
    /**
     * Le joueur qui la detient
     */
    private Joueur gouvernant;
    /**
     * Le nombre de troupes dans cette province
     */
    private int regiments;
    /**
     * Le nombre de troupes qui sont sur cette province mais ne peuvent plus agir ce tour-ci
     */
    private int regimentsFatigues;
    /**
     * Sa couleur ; pourrait etre utilise pour definir les zones cliquables par la couleur
     */
    private Color couleur;
    /**
     * Position en x du bouton associe a la province
     */
    private int layoutx;
    /**
     * Position en y du bouton associe a la province
     */
    private int layouty;

    /**
     * Generateur de province
     *
     * @param nom     Son nom
     * @param ident   Son identifiant
     * @param fort    La presence d'un fort dessus
     * @param voisins La liste des provinces voisines
     * @param couleur Sa couleur
     * @param layoutx La position en x du bouton associe a la province
     * @param layouty La position en y du bouton associe a la province
     */
    public Province(String nom, int ident, boolean fort, List<Province> voisins, Color couleur, int layoutx, int layouty) {
        super();
        this.nom = nom;
        this.fort = fort;
        this.voisins = voisins;
        this.ident = ident;
        this.couleur = couleur;
        this.layoutx = layoutx;
        this.layouty = layouty;
    }

    /**
     * Accesseur du nom
     *
     * @return le nom de la province
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Accesseur de la presence d'un fort sur la province
     *
     * @return si la province est fortifiee
     */
    public boolean isCapitale() {
        return this.fort;
    }

    /**
     * Mutateur des provinces visines
     *
     * @param voisins La liste des nouveaux voisins
     */
    public void setVoisins(List<Province> voisins) {
        this.voisins = voisins;
    }

    /**
     * Accesseur des provinces voisines
     *
     * @return les provinces voisines de la province
     */
    public List<Province> getVoisins() {
        return this.voisins;
    }

    /**
     * Accesseur du joueur qui controle la province
     *
     * @return le joueur qui controle la province
     */
    public Joueur getGouvernant() {
        return this.gouvernant;
    }

    /**
     * Mutateur du gouvernant
     *
     * @param gouvernant Le nouveau joueur maitre de la province
     */
    public void setGouvernant(Joueur gouvernant) {
        this.gouvernant = gouvernant;
    }

    /**
     * Accesseur du nombre de regiments
     *
     * @return le nombre de regiments actuellement sur la province
     */
    public int getRegiments() {
        return this.regiments;
    }

    /**
     * Mutateur du nombre de regiments
     *
     * @param regiments Le nouveau nombre de regiments sur la province
     */
    public void setRegiments(int regiments) {
        this.regiments = regiments;
    }

    /**
     * Accesseur du nombre de regiments fatigues
     *
     * @return le nombre de regiments fatigues actuellement sur la province
     */
    public int getRegimentsFatigues() {
        return this.regimentsFatigues;
    }

    /**
     * Mutateur du nombre de regiments fatigues
     *
     * @param regimentsFatigues Le nouveau nombre de regiments fatigues sur la province
     */
    public void setRegimentsFatigues(int regimentsFatigues) {
        this.regimentsFatigues = regimentsFatigues;
    }

    /**
     * Accesseur de l'identifiant
     *
     * @return l'identifiant de la province
     */
    public int getIdent() {
        return this.ident;
    }

    /**
     * Accesseur du continent
     *
     * @return le continent sur lequel la province se trouve
     */
    public Continent getContinent() {
        return this.continent;
    }

    /**
     * Mutateur du continent
     *
     * @param continent Le continent ou se trouve la province
     */
    public void setContinent(Continent continent) {
        this.continent = continent;
    }

    /**
     * Accesseur de la position en x du bouton associe
     *
     * @return la position en x du bouton associe a la province
     */
    public int getLayoutx() {
        return layoutx;
    }

    /**
     * Accesseur de la position en y du bouton associe
     *
     * @return la position en y du bouton associe a la province
     */
    public int getLayouty() {
        return layouty;
    }

    /**
     * Methode permettant d'afficher dans la console les principales caracteristiques de la province
     * (son nom, son continent, si elle est fortifiee, son proprietaire, le nombre de regiments presents
     * et, parmi eux, le nombre de regiments fatigues)
     */
    public void toStringe() {
        System.out.println(this.nom + "\n" + this.continent.getNom() + "\nFort prÃ©sent " + this.fort + "\n"
                + "\nLe gouverneur est " + this.gouvernant.getNom() +
                "\nNombre de rÃ©giments prÃ©sents sur la province : " + this.regiments +
                "\nNombre de rÃ©giments fatiguÃ©s de la province : " + this.regimentsFatigues);
    }

    /**
     * Methode permettant d'afficher dans la console les voisins de la province avec leur
     * gouvernant et un identifiant arbitraire
     */
    public void voisinsToStringe() {
        for (int i = 0; i < this.voisins.size(); i++) {
            System.out.println("Id (de la liste) : " + i + " - " + this.voisins.get(i).nom +
                    " - Le gouverneur est " + this.voisins.get(i).gouvernant.getNom());
        }
    }

    /**
     * Methode gerant l'attaque d'une province par celle-ci
     * Cette methode sert dans la version du jeu en lignes de commande
     *
     * @param cible      La province cible
     * @param armee      Le nombre de regiments avec lesquels on attaque
     * @param continents La liste des continents du jeu
     * @param pioche     La pioche
     */
    public boolean attaquer(Province cible, int armee, List<Continent> continents, Pioche pioche) {

        List<List<Integer>> ListeDesAttaque = new ArrayList<List<Integer>>();
        List<List<Integer>> ListeDesDefense = new ArrayList<List<Integer>>();

        if ((regiments - regimentsFatigues) < armee || armee > this.regiments - 1) {
            System.out.println("Vous embauchez votre cuisinier ?!");
            return false;
        }

        if (this.gouvernant.getEmpire().contains(cible)) {
            System.out.println("Mais c'est chez toi espèce de gros débile!");
            return false;
        }

        if (!(this.voisins.contains(cible))) {
            System.out.println("Trouve un dragon.");
            return false;
        }

        //bonus a la defense?
        int bonus = 0;
        if (cible.isCapitale()) {
            bonus++;
        }

        Random choixpeau = new Random();
        List<Integer> lancerDefense = new ArrayList<Integer>();
        List<Integer> lancerAttaque = new ArrayList<Integer>();

        while (armee != 0 && cible.regiments != 0) {

            System.out.println("\n");

            //Determination du nombre de des
            int nbA;
            if (armee >= 3) nbA = 3;
            else nbA = armee;

            int nbD;
            if (cible.regiments >= 2) nbD = 2;
            else nbD = cible.regiments;

            //Lancer des des
            for (int i = 0; i < nbD; i++) {
                lancerDefense.add(choixpeau.nextInt(5) + 1 + bonus);
            }
            List<Integer> lancerDefenseCopie = new ArrayList<Integer>();
            for (int i = 0; i < nbD; i++) {
                lancerDefenseCopie.add(Integer.valueOf(lancerDefense.get(i)));
            }
            ListeDesDefense.add(lancerDefenseCopie);

            //pas grave si un de est a 7,
            //car en cas d'egalite 6-6 la defense
            //aurait de toute facon gagne
            for (int i = 0; i < nbA; i++) lancerAttaque.add(choixpeau.nextInt(5) + 1);
            List<Integer> lancerAttaqueCopie = new ArrayList<Integer>();
            for (int i = 0; i < nbA; i++) {
                lancerAttaqueCopie.add(Integer.valueOf(lancerAttaque.get(i)));
            }
            ListeDesAttaque.add(lancerAttaqueCopie);

            System.out.println(lancerAttaque);
            System.out.println(lancerDefense);

            //Comparaison de lancers
            if (Collections.max(lancerAttaque) > Collections.max(lancerDefense)) {
                cible.regiments -= 1;
                System.out.println(cible.gouvernant.getNom() + " perd 1 regiment");
            } else {
                armee -= 1;
                this.regiments -= 1;
                System.out.println(this.gouvernant.getNom() + " perd 1 regiment");
            }

            lancerAttaque.remove(Collections.max(lancerAttaque));
            lancerDefense.remove(Collections.max(lancerDefense));

            if ((lancerAttaque.size() >= 1) && (lancerDefense.size() >= 1)) {
                if (Collections.max(lancerAttaque) > Collections.max(lancerDefense)) {
                    cible.regiments -= 1;
                    System.out.println(cible.gouvernant.getNom() + " perd 1 regiment");
                } else {
                    armee -= 1;
                    this.regiments -= 1;
                    System.out.println(this.gouvernant.getNom() + " perd 1 regiment");
                }
            }

            lancerDefense.clear();
            lancerAttaque.clear();
        }

        if (armee == 0) {
            System.out.println("Attaque lamentablement échouée\n");
            FenetreAttaque fifa = new FenetreAttaque(ListeDesAttaque, ListeDesDefense, ".\\graphisme", false);
            return false;
        } else {
            System.out.println("C'est gagné!");
            FenetreAttaque fifa = new FenetreAttaque(ListeDesAttaque, ListeDesDefense, ".\\graphisme", true);
            //verification de la perte du continent
            if (cible.gouvernant.getPangee().contains(cible.continent)) {
                cible.gouvernant.getPangee().remove(cible.continent);
            }
            //changement de bailleur
            cible.gouvernant.getEmpire().remove(cible);
            this.gouvernant.getEmpire().add(cible);
            cible.gouvernant = this.gouvernant;

            //deplacement
            deplacer(cible, armee);

            //verification de l'hegemonie sur un continent
            boolean hegemonie = true;
            for (Province p : cible.continent.getProvinces()) {
                if (!this.gouvernant.getEmpire().contains(p)) {
                    hegemonie = false;
                    break;
                }
            }
            if (hegemonie) this.gouvernant.getPangee().add(cible.continent);


            return true;
        }
    }


    /**
     * Methode gerant le deplacement de troupes vers une autre province
     *
     * @param cible la province cible, appartenant au meme joueur que celle de depart
     * @param armee le nombre de troupes a deplacer
     */
    public void deplacer(Province cible, int armee) {
        if ((regiments - regimentsFatigues) < armee) {
            System.out.println("Vous voulez les tuer!");
            return;
        }

        if (!this.gouvernant.getEmpire().contains(cible)) {
            System.out.println("Mais c'est pas chez toi espèce de gros débile!");
            return;
        }

        if (!(this.voisins.contains(cible))) {
            System.out.println("Trouve un dragon.");
            return;
        }

        cible.regiments += armee;
        cible.regimentsFatigues += armee;
        this.regiments -= armee;

    }
}

package Modele;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Reserve {

    /**
     * Le nombre de troupes que contient la reserve
     */
    private int caserne;
    /**
     * Le joueur proprietaire de la reserve
     */
    private Joueur joueur;

    /**
     * Generateur de reserve
     */
    public Reserve() {
        this.caserne = 0;
    }

    /**
     * Accesseur du nombre de troupes dans la reserve
     *
     * @return le nombre de troupes
     */
    public int getCaserne() {
        return caserne;
    }

    /**
     * Mutateur du nombre de troupes dans la reserve
     *
     * @param caserne Le nouveau nombre de troupes dans la reserve
     */
    public void setCaserne(int caserne) {
        this.caserne = caserne;
    }

    /**
     * Mutateur du proprietaire
     *
     * @param joueur Le nouveau proprietaire de la reserve
     */
    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
    }

    /**
     * Methode permettant d'enlever de la reserve les troupes mobilisees
     *
     * @param mobilises Le nombre de troupes mobilisees
     */
    public void viderCaserne(int mobilises) {
        this.caserne -= mobilises;
    }

    /**
     * Methode calculant le nombre de troupes en renfort supplementaires auxquelles
     * le joueur a droit, grace a sa domination sur des continents
     *
     * @return le nombre de renforts supplementaires
     */
    public int getBonusContinents() {
        int bonus = 0;
        for (Continent c : this.joueur.getPangee()) {
            bonus += c.getBonusRenfort();
        }
        return bonus;
    }

    /**
     * Methode permettant de remplir la reserve du joueur selon les regles au debut de son tour
     * Il recoit ainsi autant de troupes que le resultat de la division euclidienne du nombre
     * de provinces de son empire par 3, plus les eventuels bonus grace aux continents, et au
     * minimum 3 troupes.
     */
    public void recruter() {
        int recrues = this.joueur.getEmpire().size() / 3 + this.getBonusContinents();
        if (recrues < 3) recrues = 3;
        this.caserne += recrues;
    }

    /**
     * Methode permettant d'ajouter des renforts dans la reserve en échange de cartes bonus
     *
     * @param map Dictionnaire ayant pour cles les nombres de regiments que le joueur
     *            * 		peut obtenir, et pour valeurs les combinaisons de cartes bonus correspondantes
     */
    public void addBonusCarte(Map<Integer, List<Bonus>> map) {
        int nbBonus = 0;
        if (map.keySet().contains(3)) {
            nbBonus = 3;
        } else if (map.keySet().contains(1)) {
            nbBonus = 1;
        } else if (map.keySet().contains(5)) {
            nbBonus = 5;
        } else if (map.keySet().contains(4)) {
            nbBonus = 4;
        }
        this.caserne += nbBonus;
        this.joueur.getDeck().getMainBonus().removeAll(map.get(nbBonus));

    }

}

package Modele;

/**
 * Enumeration des categories de bataillons possibles
 *
 * @author AsTeRisk
 */

public enum TypeBataillon {
    TroupierDeBase,
    CavalierSuicidaire,
    CanonFarceur;


}

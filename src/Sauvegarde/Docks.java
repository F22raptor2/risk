package Sauvegarde;

import Modele.Bonus;
import Modele.Continent;
import Modele.Province;
import Modele.TypeBataillon;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe de chargement du monde. Elle possede des methodes qui vont charger differents fichiers afin de creer des instances de certaines classes avant le lancement
 * de chaque partie (qu'elle soit une partie chargee ou une toute nouvelle) et remplir certains de leurs attributs
 */
public class Docks {

    /**
     * Methode qui va lire le fichier issu du Filepath et va renvoyer les donnees contenues sous forme d'une liste de chaine de
     * caracteres, un element de la liste correspondant a une ligne du fichier
     *
     * @param filePath chemin vers le fichier qui va etre lu
     * @return liste des lignes du fichier sous forme d'une liste de chaines de caracteres
     */
    public List<String> loadFile(String filePath) {
        Path p = Paths.get(filePath);
        List<String> file = new ArrayList<String>();
        try (BufferedReader reader = Files.newBufferedReader(p)) {
            String line;
            while (((line = reader.readLine()) != null)) {
                file.add(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Fichier non trouvé");
        } finally {
            return (file);
        }
    }


    /**
     * Methode qui va creer la liste des provinces presentes sur la carte grace a une description des celles-ci dans la liste de chaine
     * de caractere passee en parametre
     * Elle va remplir les attributs nom, ident, fort et voisins de chaque instance de la classe province cree
     * Format de la chaine de caractere de file : Name Identifiant IdProvinceVoisine (separees par ;) Fort
     *
     * @param file donnee issus du fichier Province.txt
     * @return une liste des provinces decrites par le fichier (type List<Province>)
     */
    public List<Province> createProvince(List<String> file) {
        List<Province> provincesMonde = new ArrayList<Province>();
        List<String[]> provincesMondeVoisins = new ArrayList<String[]>();
        for (String s : file) {
            List<Province> voisins = new ArrayList<Province>();
            String[] ligne = s.split(" ");
            Province P = new Province(ligne[0], Integer.parseInt(ligne[1]), Boolean.getBoolean(ligne[3]), voisins,
                    new Color(Integer.parseInt(ligne[4]), Integer.parseInt(ligne[5]), Integer.parseInt(ligne[6]))
                    , Integer.parseInt(ligne[7]), Integer.parseInt(ligne[8]));
            provincesMonde.add(P);
            String[] vois = ligne[2].split(";");
            provincesMondeVoisins.add(vois);
        }
        int len = provincesMonde.size();
        for (int k = 0; k < len; k++) {
            for (Province vois : provincesMonde) {
                for (int j = 0; j < provincesMondeVoisins.get(k).length; j++) {

                    if ((provincesMondeVoisins.get(k)[j]).equals(String.valueOf(vois.getIdent()))) {
                        provincesMonde.get(k).getVoisins().add(provincesMonde.get(vois.getIdent() - 1));
                    }
                }
            }
        }

        return (provincesMonde);
    }

    /**
     * Methode qui va creer les continents du monde grace a une liste de provinces et aux donnees contenues dans la liste de
     * chaines de caracteres file
     * Elle va remplir les attributs nom, provinces, couleur et BonusRenfort de chaque instance de continent cree
     * Format d'une chaine de caractere de file : Name ProvincesIn Color Bonus
     *
     * @param file      donnees issus du fichier Continents.txt
     * @param provinces liste des provinces obtenue grace a la methode CreateProvince
     * @return une liste des continents decrits par le fichier (type List<Continent>)
     */
    public List<Continent> createContinents(List<String> file, List<Province> provinces) {
        List<Continent> continents = new ArrayList<Continent>();
        List<String[]> MondeContinentsProvinces = new ArrayList<String[]>();
        for (String s : file) {
            List<Province> provincesContinent = new ArrayList<Province>();
            String[] ligne = s.split(" ");
            Continent C = new Continent(ligne[0], provincesContinent, Integer.parseInt(ligne[3]));
            continents.add(C);
            String[] provinceInContinent = ligne[1].split(";");
            MondeContinentsProvinces.add(provinceInContinent);
        }

        int len = continents.size();
        for (int k = 0; k < len; k++) {
            for (Province P : provinces) {
                for (int j = 0; j < MondeContinentsProvinces.get(k).length; j++) {
                    if ((MondeContinentsProvinces.get(k)[j]).equals(String.valueOf(P.getIdent()))) {
                        continents.get(k).getProvinces().add(P);
                    }
                }
            }

        }
        return (continents);
    }


    /**
     * Methode qui va ajouter a chaque province un continent pour son attribut continent
     *
     * @param provinces  liste des provinces a modifier
     * @param continents liste des continents composés par les provinces
     */
    public void ajouteContinentAProvinces(List<Province> provinces, List<Continent> continents) {
        for (Continent C : continents) {
            for (Province P : provinces) {
                if (C.getProvinces().contains(P)) {
                    P.setContinent(C);
                }
            }
        }
    }


    /**
     * Methode qui va creer une liste de cartes bonus a partir des donnees contenues dans le fichier file
     * Elle va remplir les attributs Ident,  nomProvince et categorieBataillon
     * Format d'une chaine de caracteres de file : Ident nomProvince categorieBataillon
     *
     * @param file donnees issues du fichier Bonus.txt
     */
    public List<Bonus> creePiscineABonus(List<String> file) {
        List<Bonus> bonus = new ArrayList<>();
        for (String s : file) {
            String[] donneesBonus = s.split(" ");
            Bonus bonu = new Bonus(Integer.parseInt(donneesBonus[0]), donneesBonus[1], TypeBataillon.valueOf(donneesBonus[2]));
            bonus.add(bonu);
        }
        return (bonus);
    }

}

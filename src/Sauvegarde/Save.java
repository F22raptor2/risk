package Sauvegarde;

import Modele.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe de sauvegarde et de chargement d'une partie qui contient des methodes de chargeent et de sauvegarde des joueurs presents et de la pioche
 */
public class Save {

    /**
     * Methode qui va sauvegarder la partie dans un fichier decrit par le filePath.Si ce fichier existe deja, il va etre mis a jour.
     * Le fichier contient une liste des attribut des joueurs de la partie
     *
     * @param filePath chemin vers le fichier de sauvegarde
     */
    public void saveJoueur(String filePath, List<Joueur> joueurs, int joueurTour) {
        Path logFile = Paths.get(filePath);
        try {
            Files.deleteIfExists(logFile);
        } catch (IOException e) {

            e.printStackTrace();
        }
        try {
            Files.createFile(logFile);
        } catch (IOException e) {

            e.printStackTrace();
        }
        try (BufferedWriter writer = Files.newBufferedWriter(logFile,
                StandardCharsets.UTF_8, StandardOpenOption.WRITE)) {
            int compt = 0;
            for (Joueur j : joueurs) {
                String line = "";
                line += j.getNom() + " ";
                line += j.getCouleur() + " ";
                for (Province p : j.getEmpire()) {
                    line += p.getIdent() + "," + p.getRegiments() + "," + p.getRegimentsFatigues() + ";";
                }

                line += " ";
                line += j.getConscription().getCaserne();
                /*
               for (Evenement e : j.getDeck().getMainEvenement()) {
                    line += e.getIdent() + ",";
                }
                */

                line += " ";
                if (j.getDeck().getMainBonus().size() != 0) {
                    for (Bonus b : j.getDeck().getMainBonus()) {
                        line += b.getIdent() + ";";
                    }
                }


                line += " ";
                if (j.getPangee().size() != 0) {
                    for (Continent c : j.getPangee()) {
                        line += c.getNom() + ";";
                    }
                }
                line += " ";
                if (Integer.valueOf(joueurTour) == compt) {
                    line += true;
                }
                line += "\n";
                writer.write(line);
                compt++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Methode qui va lire un fichier de sauvegarde cree par la methode save et va modifier le monde charge pour y ajouter les joueurs
     * decrit dans le fichier en modifiant entre autre l'attribut gouvernant de chauqe province
     *
     * @param file       chemin absolue vers le fichier de sauvegarde
     * @param monde      liste des provinces du monde
     * @param listeBonus liste de toutes les cartes bonus presentent dans le jeu
     * @param continents liste de tous les continents du jeu
     * @param joueurs    liste des joueurs presents dans le jeu (est une liste vide pour l'instant)
     */
    public int loadJoueur(List<String> file, List<Province> monde, List<Bonus> listeBonus,
                          List<Continent> continents, List<Joueur> joueurs) {
        int joueurTour = 0;
        int compt = 0;
        for (String s : file) {
            List<Province> empire = new ArrayList<>();
            String couleurJoueur = null;
            String nomJoueur = "";
            Reserve reserve = new Reserve();
            String[] donneeJoueur = s.split(" ");
            List<Continent> pangee = new ArrayList<>();
            //Creation d'un joueur type
            Joueur j = new Joueur("", "Bleu", empire, reserve, new Deck(), pangee);
            for (int k = 0; k < donneeJoueur.length; k++) {
                //changement du nom du joueur
                if (k == 0) {
                    nomJoueur = donneeJoueur[k];
                    j.setNom(nomJoueur);
                    //changement de la couleur du joueur
                } else if (k == 1) {
                    couleurJoueur = donneeJoueur[k];
                    j.setColor(couleurJoueur);
                    //changement des attributs gouvernant, regiment et regimentsfatigues de toutes les provinces appartenant a ce joueur
                } else if (k == 2) {
                    String[] listDonneeProvinces = donneeJoueur[k].split(";");
                    for (String sP : listDonneeProvinces) {
                        String[] donneeProvince = sP.split(",");
                        Province p = monde.get(Integer.valueOf(donneeProvince[0]) - 1);
                        p.setRegiments(Integer.valueOf(donneeProvince[1]));
                        p.setRegimentsFatigues(Integer.valueOf(donneeProvince[2]));
                        empire.add(p);
                        p.setGouvernant(j);
                    }
                    //changement de la valeur de la reserve du joueur
                } else if (k == 3) {
                    reserve.setCaserne(Integer.valueOf(donneeJoueur[k]));
                    reserve.setJoueur((j));
                }
                //changement de la mainBonus du joueur
                else if (k == 4) {
                    String[] listDonneeMain = donneeJoueur[k].split(";");
                    if (!listDonneeMain[0].equals("")) {
                        for (int i = 0; i < listDonneeMain.length; i++) {
                            j.getDeck().getMainBonus().add(listeBonus.get(Integer.valueOf(listDonneeMain[i]) - 1));
                        }
                    }
                }
                //changement de l'attribut pangee du joueur
                else if (k == 5) {
                    String[] listDonneeContinent = donneeJoueur[k].split(";");
                    for (String sC : listDonneeContinent) {
                        for (Continent C : continents) {
                            if (C.getNom().equals(sC)) {
                                pangee.add(C);
                            }
                        }
                    }
                } else if (k == 6) {
                    if (donneeJoueur[k].equals("true")) {
                        joueurTour = compt;
                    }
                }
            }
            joueurs.add(j);
            compt++;
        }
        return joueurTour;
    }

    /**
     * Methode de sauvegarde de l'etat de la pioche dans la partie, ie les cartes qu'elle contient
     * Le fichier creer contient la liste des identifiants des cartes bonus dans sa premiere ligne et ceux des cartes evenements dans la seconde
     *
     * @param filePath chemin absolue qui va contenir le fichier de sauvegarde
     * @param pioche   pioche qui va etre sauvegardee
     */
    public void savePioche(String filePath, Pioche pioche) {
        Path logFile = Paths.get(filePath);
        if (!Files.exists(logFile)) {
            try {
                Files.createFile(logFile);
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
        try (BufferedWriter writer = Files.newBufferedWriter(logFile,
                StandardCharsets.UTF_8, StandardOpenOption.WRITE)) {
            String line = "";
            for (Bonus b : pioche.getPonussss()) {
                line += b.getIdent() + ";";
            }
            writer.write(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Methode qui va charger la pioche associee a une partie grace aux information contenue dans un fichier creer par la methode savePioche
     *
     * @param donneesPioche donnees sur l'etat de la pioche, contenue sous la forme d'une liste des identifiants des cartes
     * @param pioche        pioche vide qui est initialisee prealablement (est une liste vide)
     */
    public void loadPioche(List<Bonus> listeBonus, List<String> donneesPioche, Pioche pioche) {
        List<Bonus> bonusPioche = new ArrayList<>();
        String[] donnees = donneesPioche.get(0).split(";");
        for (String s : donnees) {
            bonusPioche.add(listeBonus.get(Integer.valueOf(s) - 1));
        }
        pioche.setPonussss(bonusPioche);
    }


}

